<?php

function _wpr_confirm_subscriber($id,$hash,$whetherToSendConfirmationEmail = true)
{	
	global $wpdb;
	ob_end_clean();
	header("Connection: close\r\n");
	header("Content-Encoding: none\r\n");
	ignore_user_abort(true); // optional
	ob_start();
	
	$query = "UPDATE ".$wpdb->prefix."wpr_subscribers set confirmed=1,  active=1 where id=$id;";
	
	$wpdb->query($query);
	$redirectionUrl = get_bloginfo("home")."/?wpr-confirm=2";
	
	$subscriber = _wpr_subscriber_get($id);
	_wpr_move_subscriber($subscriber->nid,$subscriber->email);
	
	
	//This subscriber's follow up subscriptions' time of creation should be updated to the time of confirmation. 
	$currentTime = time();
	$query = "UPDATE ".$wpdb->prefix."wpr_followup_subscriptions set doc='$currentTime', last_date='$currentTime' where sid=$id;";
	$wpdb->query($query);
	if ($whetherToSendConfirmationEmail == true)
		sendConfirmedEmail($id);	

	
	return $redirectionUrl;
}


?>