<?php
function _wpr_network()
{
	
	
	if (isset($_POST['_wpr_network_return_path_form']))
	{
		$the_return_path = $_POST['return_email'];
		
		if (is_email($the_return_path))
		{
			update_option("_wpr_network_return_path",$the_return_path);
		}
		else
		{
			$error=1;
		}
	}
	$return_path = get_option("_wpr_network_return_path");
	
	?>
    <div class="wrap">    
    <h2>WP Responder Network Settings</h2>
    
    <?php if ($error == 1)
	{
		
		?>
        <div class="error">        
        	The entered string is not an email address. Please enter an email address.
        </div>
        <?php
		
	}
	?>
    <form action="<?php print $_SERVER['REQUEST_URI'] ?>" method="post">
     Global Return-Path E-mail Address: <input type="text" name="return_email" value="<?php echo $return_path ?>"/>
     <input type="submit" name="submit" class="button-primary" value="Save" />
     <input type="hidden" name="_wpr_network_return_path_form" value="1" />
     </form>
    </div>
    <?php
}