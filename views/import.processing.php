<div class="wrap">

<h2>Subscribers Import: Processing</h2>

<big>The subscribers have been uploaded to the server and are being imported. Please check the <a href="admin.php?page=wpresponder/importexport">subscriber import home</a> to see the progress of the import or to speed up the import job. 

</big>

<p><p>Go to the <a href="admin.php?page=wpresponder/subscribers.php">subscribers management</a> page to see the results.

<p></p>
<a href="admin.php?page=wpresponder/importexport" class="button-primary">Import/Export Home</a>
<a href="admin.php?page=wpresponder/subscribers.php" class="button-primary">Subscribers Management</a>
