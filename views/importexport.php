<?php

?><div class="wrap">
    <h2>Import/Export Subscribers</h2>

    <h3>Export Subscribers</h3>
    <table class="widefat" style="width: 500px;">
        <thead>
            <tr>
                <th>Email System</th>
                <th>Download</th>
            </tr>
        </thead>
        <?php
        foreach ($newslettersList as $newsletter)
        {
        ?>
        <tr>
            <td><?php echo $newsletter->name ?></td>
            <td>
            <form action="admin.php?page=wpresponder/importexport" method="post">
            <input type="hidden" name="wpr_form" value="wpr_subscriber_export" />
            <input type="hidden" name="newsletter" value="<?php echo $newsletter->id ?>" />
            <input type="submit" value="Download" class="button-primary">            
            </form></td>
            
        </tr><?php
        }
        ?>
        </thead>
    </table>
</div>
<h3>Import Subscribers</h3>

Import a subscriber to your newsletter. Import from any service or program - Feedburner, Aweber, Feedblitz, etc. You can import your subscribers if you have the subscriber list in a CSV format. Select the newsletter below to import subscribers:

<?php
if (isset($importerrors))
{
    ?>
<div style="border: 1px solid #000; background-color: #ff9; padding: 20px;">
    <ul>
        <?php
        foreach ($importerrors as $index=>$error)
        {
            ?>
        <?php echo $index+1 ?>. <?php echo $error ?><br/>
       <?php
        }
        ?>
    </ul>
</div>
<?php
}
?>
<p></p>
<?php
if (count($newslettersList))
{
	?>
<form action="admin.php?page=wpresponder/importexport/import/step1" method="post" enctype="multipart/form-data">
<script>
var relationship = <?php echo json_encode($autoresponderNewsletterList); ?>;

function autoresponderField() {
	return document.getElementById('autoresponder_list');
}

function addAutoresponder(id, name)
{
	var select = autoresponderField();
	var option=document.createElement("option");
	option.setAttribute("value",id);
	option.innerHTML = name;
	select.appendChild(option);
}

function clearAutorespondersList() 
{
	var selectList = autoresponderField();
	selectList.options.length=0;
}

function changeAutoresponderList(newsletterField)
{
	var nid = newsletterField.value;
	 clearAutorespondersList();
	 addAutoresponder('','');
	if (nid == '')
	   return;
	if (relationship[nid])
	{
		autoresponders = relationship[nid];
		for (var iter in autoresponders)
		{
			addAutoresponder(autoresponders[iter].id,autoresponders[iter].name);
		}
	}
}
</script>
<table width="700">
  <tr> 
     <td>Select a email system: </td>
	 <td><select name="nid" onchange="changeAutoresponderList(this)">
	 <option selected="selected"></option>
		<?php
		foreach ($newslettersList as $newsletter)
		{
		?>
		<option <?php if ($_SESSION['wpr_import_newsletter']== $newsletter->id) echo 'selected="selected"'; ?> value="<?php echo $newsletter->id ?>"><?php echo $newsletter->name ?></option>
		<?php	
		}
		?>
</select></td>
<tr>
  <td>
  Select a autoresponder:
  </td>
  <td>
	<select name="aid" id="autoresponder_list">	
        <option></option>
	</select>
  </td>
</tr>
<tr>
  <td>
  Select a post series to subscribe these subscribers to:
  </td>
  <td>
	<select <?php if (count($postseriesList) == 0 ) echo 'disabled="disabled';  ?> name="pid" id="postseries_select">	  
            <option></option>
	<?php
	if (count($postseriesList) > 0 ) {
		foreach ($postseriesList as $series) {
			?>
			<option value="<?php echo $series->id ?>"><?php echo $series->name ?></option>
			<?php
		}
	}
	else
	{
	   ?><option>No Post Series Defined</option><?php
	}?></select>
  </td>
</tr>
<tr>
  <td>
  Select a blog subscription:
  </td>
  <td>
	<select name="blog" id="postseries_select">
            <option></option>
	<option value="all">Subscribe to all blog posts published on the blog</option>
	<optgroup label = "Subscribe to category:">
	<?php foreach ($categories as $category) 
	{
		?>
		<option value="<?php echo $category->term_id ?>"><?php echo $category->name?></option>
		<?php
	}
	?>
	</select>
  </td>
</tr>
<tr>
   <td>CSV File: </td>
   <td>
   <input type="hidden" name="MAX_FILE_SIZE" value="2097152"/>
   <input type="file" name="csv" /> <br/>(Maximum file size supported: 2MB)</td>
</tr>
</table>
<script>
</script>

<input type="hidden" name="wpr_form" value="wpr_import_first" />
<input class="button-primary" type="submit" value="Next &raquo;" />
</form>

<h3>Currently pending Import Jobs</h3>

<table class="widefat">
    <thead>
    <tr>
        <th width="40">S.No. </th>
        <th width="400">Settings</th>
        <th width="50">Pending</th>
        <th width="50">Finished</th>
        <th>Notes</th>
        <th>% completion</th>
        <th>Force</th>
        <th>Trash</th>
    </tr>
    </thead>
    <?php
    $count =1;
    if (count($pending_import_jobs) > 0)
    {
        foreach ($pending_import_jobs as $index=>$job)
        {
            ?>
        <tr>
            <td><?php echo $count ?></td>
            <td><?php  echo $job['settings']; ?></td>
            <td><?php  print_r($job['pending']); ?></td>
            <td><?php  print_r($job['finished']);
            $report = $job['report'];
            ?></td>
            <td><strong>Imported emails:</strong> <?php echo  (int) $report['Imported']; ?><br/>
<strong>Already existing updated:</strong> <?php echo (int) $report['Updated']; ?> <br/>
<strong>Invalid email addresses: </strong><?php echo (int) $report['Invalid']; ?> <br/>

                
            </td>
            <td><?php  echo ceil(round($job['finished']/($job['finished']+$job['pending']),2)*100); ?></td>
            <td>
                <form action="admin.php?page=wpresponder/importexport" method="post">
                    <input type="submit" class="button-primary" value="Force import of 1000"/>
                    <input type="hidden" name="key" value="<?php echo $index ?>"/>
                    <input type="hidden" name="wpr_form" value="force_import"/>
                </form>
            </td>
            <td>
                <form action="admin.php?page=wpresponder/importexport" method="post">
                    <input type="submit" class="button-primary" onclick="return window.confirm("Are you sure you want to abort this import job?");" value="Abort further import"/>
                    <input type="hidden" name="key" value="<?php echo $index ?>"/>
                    <input type="hidden" name="wpr_form" value="trash_import"/>
                </form>
            </td>
        </tr>
        <?php
        $count++;
        }
    }
    else
    {
        ?>
        <tr>
            <td colspan="10"><center>--No Pending Jobs-- </center></td>
        </tr>
        <?php
    }
    ?>
</table>
<?php

}
else
{
	?>
    <strong>No newsletters created. Create a email system before importing subscribers</strong>
    <?php
}?>

  
