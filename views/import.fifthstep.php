<?php
/*
 * View expects:
 * $list = array of row arrays
 * $columns = array of field names - array(
 *                                            '-1'=> 'Name',
 *                                            '0' => 'Email',
 *                                            '1'=> 'nameofcf',
 *                                         )
 */
?>
<div class="wrap">

    <h2><?php _e("Import Subscribers: Identify Columns"); ?></h2>

    <?php _e("Identify the columns in the csv file. Shown below are the first 100 rows. "); ?>
    <form action="<?php echo $_SERVER['REQUEST_URI'] ?>" method="post">
        <?php
        
        if (count($errors))
        {
            ?>
        <div style="padding: 10px; background-color: #ff9;">
            <?php
            foreach ($errors as $index=>$error)
            {
        ?>
<?php echo $error ?><br/>
        <?php
  
            }
            ?>
        </div><?php
        }
        
if (count($list) > 0)
{
    $columnCount =  count($list[0]);
        ?>
        <table class="widefat" style="width: auto;">
            <thead>
            <tr>
                <th></th>
                    <?php
                    for ($i=0;$i<$columnCount;$i++)
                    {
                ?><th>
                    <select class="colfield" name="column_<?php echo $i ?>">
                        <option></option>
                    <?php
                        foreach ($columns as $name=>$label)
                        {
                            ?>
                        <option value="<?php echo $name ?>"><?php echo $label ?></option>
                        <?php
                        
                        }
                        ?>
                    </select>
                </th>
                <?php
                    }

                    ?>
                
            </tr>
            </thead>
            <?php
            foreach ($list as $row)
            {
                ?>
            <tr>
                <td></td>
                <?php for ($i=0;$i<$columnCount;$i++)
                {
                  ?>
                <td><?php echo $row[$i]; ?></td>
                <?php
                }
                ?>
            </tr>
            <?php
            }

?>

        </table>
        <?php
}
else
{
    ?><?php _e("The CSV File your provided was incomprehensible. Please modify it or use a different file."); ?><?php
}
    ?> 
        <input type="hidden" name="wpr_form" value="wpr_import_finish" />
        <input onclick="var result= _wpr_fifth_validateForm(); if (result) { alert('<?php _e('Importing may take a long time to finish. Please be patient. DO NOT press REFRESH or STOP the loading of the page. ') ?>');} return result; " type="submit" value="Next: Finish &raquo;" class="button-primary">
    </form>    
</div>

<script>
    
    function in_array (needle, haystack, argStrict) {    
    var key = '',
        strict = !! argStrict;
 
    if (strict) {
        for (key in haystack) {
            if (haystack[key] === needle) {
                return true;
            }
        }
    } else {
        for (key in haystack) {
            if (haystack[key] == needle) {
                return true;
            }
        }
    }
 
    return false;
}
    var fields = <?php echo json_encode($columns); ?>

    function _wpr_fifth_validateForm()
    {
        var column_field_relations=[];
        var index=0;
        var eles = jQuery(".colfield");
        for (var index=0;index<eles.length;index++)
        {
            var currValue = eles[index].children[eles[index].selectedIndex].value;
            if (currValue=='')
                continue;
            if (in_array(currValue, column_field_relations, false))
                {
                    alert("<?php  _e("One of the fields has been selected more than once."); ?>");
                    return false;
                }
            column_field_relations.push(currValue);
        }
        
        
        if (!in_array(-1,column_field_relations))
            {
                alert("<?php _e("A column has not been selected for the name field."); ?>");
                return false;    
            }
            
        if (!in_array(0,column_field_relations))
        {
            alert("<?php _e("A column hs not been selected for the e-mail address field") ?>");
            return false;
        }
        return true;
    }
</script>