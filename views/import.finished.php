<div class="wrap">

<h2>Import Finished</h2>

The subscribers have been imported to the newsletter <?php echo $newsletter->name ?> with the subscription option you specified. 

<p></p>
<p></p>
<h3>Import Settings</h3>
<strong>Imported newsletter:</strong> <?php echo $newsletter_name; ?><br/>
<strong>Autoresponder Subscription:</strong> <?php echo $autoresponder  ?><br/>
<strong>Post Series Subscription:</strong> <?php echo $postseries  ?><br/>
<strong>Blog Subscription:</strong> <?php echo $blogsubs; ?><br/>
<br/>
<h3>Import Report:</h3>
<strong>Total number of email addresses imported:</strong> <?php echo  (int) $report['Imported']; ?><br/>
<strong>Total number of already existing email addresses that were updated:</strong> <?php echo (int) $report['Updated']; ?> <br/>
<strong>Total number of invalid email addresses: </strong><?php echo (int) $report['Invalid']; ?> <br/>



<p><p>Go to the <a href="admin.php?page=wpresponder/subscribers.php">subscribers management</a> page to see the results.

<p></p>
<a href="admin.php?page=wpresponder/importexport" class="button-primary">Import/Export Home</a>
<a href="admin.php?page=wpresponder/subscribers.php" class="button-primary">Subscribers Management</a>