<div class="wrap">

    <form action="admin.php?page=addsub/<?php echo $newsletter->id ?>" method="post">


        <table>
            <tr>
                <td>Name :</td>
                <td><input type="text" name="name" value=""/></td>
            </tr>
            <tr>
                <td>Email :</td>
                <td><input type="text" name="name" value=""/></td>
            </tr>
            <tr>
                <td>Follow-up Subscription :</td>
                <td>
                    <select name="followup">
                    <?php

                    foreach ($autoresponders as $autorespodner)
                    {
                        ?>
                        <option value="autoresponder_<?php echo $autoresponder->id ?>">'<?php echo $autoresponder->name ?>' autoresponder</option>
                        <?php
                    }

                    foreach ($postseries as $series)
                    {
                        ?>
                        <option value="postseries_<?php echo $series->id ?>">'<?php echo $series->name ?>' post series</option>
                        <?php
                    }
                    ?>
                        </select>
                </td>
            </tr>
            <?php
            foreach ($customfields as $field)
            {
                ?>
            <tr>
                <td><?php echo $field->label ?></td>
            <td>
            <?php
                if ($field->type=="enum")
                {
                    $options = explode(",",$field->enum);
                    ?>
                    <select name="cus_<?php echo $field->id ?>">
                        <?php
                        foreach ($options as $option)
                        {
                            ?><option><?php echo $option->value; ?></option>
                            <?php
                        }
                        ?>
                    </select>
                    <?php
                }
                else
                {
                    ?>
                <input type="text" name="cus_<?php echo $field->id ?>"/>
                <?php                   
                }
            ?>
            </td>
            </tr>
            <?php

            }
            ?>


        </table>


    <input type="hidden" name="wpr_form" value="wpr_addsub_form"/>
    </form>


</div>
