<?php
ob_end_clean();
header("Connection: close\r\n");
header("Content-Encoding: none\r\n");
ignore_user_abort(true); // optional
ob_start();

global $wpdb;
include_once "subscriber.lib.php";
include_once "confirm.lib.php";


$string = $_GET['wpr-confirm'];
$args = base64_decode($string);

$args = explode("%%",$args);

$id = (int) $args[0];
$hash = trim(strip_tags($args[1]));

if (get_magic_quotes_gpc()==1)
{
    $hash = addslashes($hash);
}
$redirectionUrl = _wpr_confirm_subscriber($id,$hash,true);

?><script>
window.location='<?php echo $redirectionUrl ?>';
</script><?php


$size = ob_get_length();
header("Content-Length: $size");
ob_end_flush();     
flush();            
ob_end_clean();

_wpr_autoresponder_process(true);

exit;

