<?php
/*
Plugin Name: WP Responder for TotalAutoPilot.com
Plugin URI: http://www.wpresponder.com
Description: Plugin to add follow-up autoresponder sequence module to TotalAutoPilot.com
Version: 4.9.5.1.8
Author: Raj Sekharan
Author URI: http://www.krusible.com/
*/

if (!defined("WPR_DEFS"))
{
     define("WPR_DEFS",1);
	$phpVersion = phpversion();
     if (preg_match("@^4.*@",$phpVersion))
     {
          add_action('admin_notice',"wpr_unsupported");
     }
     else
	 {
	    $plugindir =  str_replace(basename(__FILE__),"",__FILE__);
        $plugindir = str_replace("\\","/",$plugindir);
        $plugindir = rtrim($plugindir,"/");

		include "wpr_install.php";
		include "home.php" ;
		include "newsletter.php";
		include "autoresponder.php";
		include "blog_series.php";
		include "forms.php";
		include "newmail.php";
		include "customizeblogemail.php";
		include "subscribers.php";
		include "wpr_settings.php";
		include "wpr_deactivate.php";
		include "all_mailouts.php";
		include "actions.php";
		include "runcronnow.php";
		include "wpr_network.php";
		include "errors.php";
		include "thecron.php";

		include $plugindir."/lib/swift_required.php";
		include "widget.php";
		define("WPR_VERSION","4.9.5.1");

		//include all files in the controller directory.
		$controllerDir = str_replace(basename(__FILE__),"",__FILE__);
		$controllerDir = rtrim($controllerDir,"\/");
		$controllerDir = "$controllerDir/controllers";
		$controllerDirectoryPtr = opendir($controllerDir);
		while ($file = readdir($controllerDirectoryPtr))
		{
			if (is_file("$controllerDir/$file") && preg_match("@\.php$@",$file))
				include "$controllerDir/$file";
			
		}

		$wpr_globals = array();

		function _wpr_get($name)
		{
			global $wpr_globals;
			if (isset($wpr_globals[$name]))
				  return $wpr_globals[$name];
			else
			return null;
		}

		function _wpr_set($name,$value)
		{
			global $wpr_globals;
			$wpr_globals[$name] = $value;
		}
	
	
	$currentDir = str_replace("wpresponder.php","",__FILE__);
	$plugindir = basename($currentDir);
	define("WPR_PLUGIN_DIR","$plugindir");
	$GLOBALS['_wpr_cache'] = array();
	
	function whetherToNag()
	{
		$address = get_option("wpr_address");		
		if (!$address && is_admin() && current_user_can('activate_plugins'))  
		{
			add_action("admin_notices","no_address_error");	
		}
	}
	
	if (!function_exists("wpr_sanitize"))
	{
					
			function wpr_sanitize($string)
			{
				$string = strip_tags($string);
				$string = trim($string);
				if (get_magic_quotes_gpc())
				{
					return $string;	
				}
				else
				{
					return addslashes($string);	
				}
			}
		}

	
	
		function validateEmail($email)
		{
			//test with regular expressions.
			return eregi('^[a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\.([a-zA-Z]{2,4})$',$email);
		}
		
	function _wpr_export_escape_field_value(&$item,$key)
	{
                $item = str_replace("\"","\"\"",$item);
		$item = "\"$item\"";
	}
	add_action("plugins_loaded","whetherToNag");
	function export_csv($nid)
	{
		global $wpdb;
		$prefix = $wpdb->prefix;

		
		set_time_limit(0);
		
		$nid = intval($nid);
		if ($nid == 0)
			return;
		//does the newsletter exist?
		$query = "SELECT COUNT(*) FROM ".$prefix."wpr_newsletters where id=$nid";
		$results = $wpdb->get_results($query);
		if (count($results) == 0)
		{

			exit;
		}
		//if none of these error conditions occur, then start exporting:
		
		//fetch all custom fields associates with this newsletter
		$query = "select * from ".$prefix."wpr_custom_fields where nid=$nid";
		$results = $wpdb->get_results($query);
		$fieldHeaders = array();
		if (count ($results))
		{
			$customfields= array();
			foreach ($results as $field)
			{
				$customfields[] = $field->name;

			}
		}
		//get all the custom fields of this newsletter:
		
		$getCustomFieldsQuery = sprintf("SELECT * FROM %swpr_custom_fields WHERE nid=%d ORDER by id",$wpdb->prefix,$nid);
		//get all the custom field values for the first 1000 subscribers. 
		$customFields= $wpdb->get_results($getCustomFieldsQuery);
		
		//form the array for the custom field row names
		
		$fields = array();
		
		foreach ($customFields as $field) {
			$fields[$field->id] = $field->label;
		}
                
                
                


		//number of susbcribers
		$getNumberOfSubscribersQuery = sprintf("SELECT COUNT(*) num FROM %swpr_subscribers WHERE nid=%d",$wpdb->prefix,$nid);
		$numberOfSubscribersRes = $wpdb->get_results($getNumberOfSubscribersQuery);
		$number = $numberOfSubscribersRes[0]->num;
		
		$perIterationSize=100;
		//process 100 subscribers at a time
		$numberOfIterations = ceil($number/$perIterationSize);
		$index = 0;
		header ("Content-disposition: attachment; filename=export_$nid.csv");
			
		//output the header
		$fieldNamesArray = $fields;
		//array_walk($fieldNamesArray, "_wpr_export_escape_field_value");
		//process 100 at a time
		
		
        $fp = fopen("php://output","w");
		array_unshift($fieldNamesArray,"E-mail");
		array_unshift($fieldNamesArray,"Name"); 
                fputcsv($fp,$fieldNamesArray);
		for ($iter=0;$iter<$numberOfIterations;$iter++)
		{

			$start = $perIterationSize*$iter;
			
			$runtimeTableQuery = sprintf("SELECT * FROM %swpr_subscribers WHERE nid=%d ORDER BY id LIMIT %d, %d",$wpdb->prefix,$nid,$start, $perIterationSize);

			$subscribers = $wpdb->get_results($runtimeTableQuery);
			
			foreach ($subscribers as $subscriber) {
				
				$getCustomFieldValuesQuery = sprintf("SELECT * FROM %swpr_custom_fields_values WHERE sid=%d ORDER BY cid",$wpdb->prefix, $subscriber->id);
				
				$customFieldValues= $wpdb->get_results($getCustomFieldValuesQuery);
				
				$current = array();
				foreach ($customFieldValues as $id=>$value) {
					$current[$value->cid]=$value->value;
				}
				
				//array_walk($current,"_wpr_export_escape_field_value");
				//form the array of id value pairs
				$valueSet = array();
				$valueSet[] = $subscriber->name;
				$valueSet[] = $subscriber->email;
				
				foreach ($fields as $cid=>$name) 
				{
					$valueSet[]= $current[$cid];
				}
				fputcsv($fp,$valueSet);
			}
		}
		exit;
	
	}
	function wpr_services_notice()
	{ //this images is used to announce new versions. DO NOT REMOVE THIS. 
	?>
<a href="http://www.expeditionpost.com/redirect.php"><img src="http://www.expeditionpost.com/wpresad-<?php echo WPR_VERSION; ?>.gif" /></a><br />
<?php
	}
	
	function no_address_error()
	{
	
		echo '<div class="error fade" style="background-color:red; line-height: 20px;"><p><strong>You must set your address in the  <a href="' . admin_url( 'admin.php?page=wpresponder/settings.php' ) . '">settings page</a> to avoid spam complaints and avoid the e-mails you send being flagged as spam. <br />
	
	<br />
	
	(and being sent to prison for breaking the law!) </strong></p></div>';	
	}
	
	function _wpr_no_newsletters($message)
	{
	
		global $wpdb;
	
		$query = "SELECT * FROM ".$wpdb->prefix."wpr_newsletters"; 
	
		$countOfNewsletters = $wpdb->get_results($query);
	
		$count = count($countOfNewsletters);
	
		unset($countOfNewsletters);
	
		if ($count ==0)
	
		{
	
			?>
<div class="wrap">
  <h2>No Email Systems Created Yet</h2>
</div>
<?php echo $message ?>, you must first create an email system. <br />
<br/>
<a href="admin.php?page=wpresponder/newsletter.php&act=add" class="button">Create Email System</a>
<?php
	
			return true;
	
		}
	
		else
	
			return false;
	
	}
	
	
	function wpr_enqueue_post_page_scripts()
	{
		
		if (isset($_GET['post_type']) && $_GET['post_type'] == "page")
		{
			return;		
		}
		$directory = str_replace("wpresponder.php","",__FILE__);
		$containingdirectory = basename($directory);
		 wp_enqueue_style("wpresponder-tabber","/".PLUGINDIR."/".$containingdirectory."/tabber.css");
		 wp_enqueue_script("wpresponder-tabber");
		 wp_enqueue_script("wpresponder-ckeditor");
		 wp_enqueue_script("wpresponder-addedit");		 
		 wp_enqueue_script("jquery");
	}
	
	function wpr_enqueue_admin_scripts()
	{
		if (is_admin() && current_user_can('level_8'))
		{
			wp_enqueue_script('jquery');
			$directory = str_replace("wpresponder.php","",__FILE__);
			$containingdirectory = basename($directory);
			if (preg_match("@wp-responder-email-autoresponder-and-newsletter-plugin/[^.]*.php@",$_SERVER['REQUEST_URI']) || preg_match("@wpresponder/[^.]*.php@",$_SERVER['REQUEST_URI']))
			{
					wp_enqueue_script('wpresponder-uis',"/".PLUGINDIR."/".$containingdirectory."/jqueryui.js");
					wp_enqueue_style("wpresponder-ui-style","/".PLUGINDIR."/".$containingdirectory."/jqueryui.css");		
					wp_enqueue_style("wpresponder-style","/".PLUGINDIR."/".$containingdirectory."/style.css");
			}
			$url = $_SERVER['REQUEST_URI'];
			if (preg_match("@newmail\.php@",$url) || preg_match("@autoresponder\.php@",$url)|| preg_match("@allmailouts\.php\&action=edit@",$url))
			{
				wp_enqueue_script("wpresponder-ckeditor");
				wp_enqueue_script("jquery");
			}
			//if the current request is for exporting a csv then run the export!				
			//add the add post area.
		}
	}
	
	function wpresponder_init_method() 
	{
		//load the scripts only for the administrator.
		global $current_user;
		session_start();		
		
		/*     Attaching the functions to the Crons     */
		add_action('admin_menu', 'wpr_admin_menu');
		//the cron that processes emails
		add_action('wpr_cronjob','wpr_processEmails');
		//the cron that delivers email. 
		add_action('wpr_queue','wpr_processqueue');
		//the tutorial series

                
	
		//whats the point you ask? 
		@ini_set( 'upload_max_size' , '100M' );
		@ini_set( 'post_max_size', '105M');
		@ini_set( 'max_execution_time', '300' );
                
                
                
                _wpr_delete_abandonded_imports();


                /*
                 * This is needed until all the pages are migrated to the
                 * MVC format. 
                 */
			if (isset($_GET['page']) && preg_match("@^wpresponder/.*@",$_GET['page']))
			{
				_wpr_handle_post();
			}
		
		$option = get_option("timezone_string");
		
		//a visitor is trying to subscribe.
		if (isset($_GET['wpr-optin']) && $_GET['wpr-optin'] == 1)
		{
			require "optin.php";			
			exit;
		}
		
		if (isset($_GET['wpr-optin']) && $_GET['wpr-optin'] == 2)
		{
			require "verify.php";	
			exit;
		}
		
		//a subscriber is trying to confirm their subscription. 
		if (isset($_GET['wpr-confirm']) && $_GET['wpr-confirm']!=2)
		{
			include "confirm.php";			
			exit;
		}
		$directory = str_replace("wpresponder.php","",__FILE__);
		$containingdirectory = basename($directory);
		wp_register_script( "wpresponder-tabber", "/".PLUGINDIR."/".$containingdirectory."/tabber.js");
		wp_register_script( "wpresponder-ckeditor", "/".PLUGINDIR."/".$containingdirectory."/ckeditor/ckeditor.js");
		wp_register_script( "wpresponder-addedit", "/".PLUGINDIR."/".$containingdirectory."/script.js");
		if (isset($_GET['wpr-confirm']) && $_GET['wpr-confirm']==2)
		{
			include "confirmed.php";
			exit;
		}
		
		if (isset($_GET['wpr-manage']))
		{
			include "manage.php";
			exit;
		}
		
		if (isset($_GET['wpr-admin-action']) )
		{
			switch ($_GET['wpr-admin-action'])
			{
				case 'preview_email':
					include "preview_email.php";
					exit;
				break;
				case 'view_recipients':
					include("view_recipients.php");
					exit;
				break;
				case 'filter':
					include("filter.php");
					exit;
				break;
				case 'delete_mailout':
				
				include "delmailout.php";
				exit;
				
				break;
				case '':
				
				break;
				
			}
			
		}
		
		
		if (isset($_GET['wpr-template']))
		{
			include "templateproxy.php";
			exit;
			
		}
	
	 add_action('admin_init','wpr_enqueue_admin_scripts');
	 add_action('admin_menu', 'wpresponder_meta_box_add');
	 add_action('edit_post', "wpr_edit_post_save");
	 add_action('load-post-new.php','wpr_enqueue_post_page_scripts');
	 add_action('admin_action_edit','wpr_enqueue_post_page_scripts');
	 add_action('publish_post', "wpr_add_post_save");
		
		
	
	}    
	add_action('widgets_init','wpr_widgets_init');
	add_action('init', "wpresponder_init_method");
	register_activation_hook(__FILE__,"wpresponder_install");
	register_deactivation_hook(__FILE__,"wpresponder_deactivate");
        
	$url = $_SERVER['REQUEST_URI'];
	
	function wpr_admin_menu()
	{
		add_menu_page('Email Systems','Email Systems','activate_plugins',__FILE__);
		add_submenu_page(__FILE__,"Dashboard","Dashboard",'activate_plugins',__FILE__,"wpr_dashboard");
		add_submenu_page(__FILE__,'New Broadcast','New Broadcast','activate_plugins',"wpresponder/newmail.php","wpr_newmail");
		add_submenu_page(__FILE__,'All Broadcasts','All Broadcasts','activate_plugins',"wpresponder/allmailouts.php","wpr_all_mailouts");
		add_submenu_page(__FILE__,'Email Systems','Email Systems','activate_plugins',"wpresponder/newsletter.php","wpr_newsletter");
			add_submenu_page(__FILE__,'Custom Fields','Custom Fields','activate_plugins',"wpresponder/custom_fields.php","wpr_customfields");
		add_submenu_page(__FILE__,'Subscription Forms','Subscription Forms','activate_plugins',"wpresponder/subscriptionforms.php","wpr_subscriptionforms");
		add_submenu_page(__FILE__,'Post Series','Post Series','activate_plugins',"wpresponder/blogseries.php","wpr_blogseries");
		add_submenu_page(__FILE__,'Autoresponders','Autoresponders','activate_plugins',"wpresponder/autoresponder.php","wpr_autoresponder");
		add_submenu_page(__FILE__,'Subscribers','Subscribers','activate_plugins',"wpresponder/subscribers.php","wpr_subscribers");
        add_submenu_page(__FILE__,'Actions','Actions','activate_plugins',"wpresponder/actions.php","wpr_actions");
		add_submenu_page(__FILE__,'Settings','Settings','activate_plugins',"wpresponder/settings.php","wpr_settings");		

        if (preg_match("@^wpresponder/importexport/.*@",$_GET['page']))
		{
                   add_submenu_page(__FILE__,'Import/Export Subscribers','Import/Export Subscribers','activate_plugins','wpresponder/importexport',"_wpr_handle_request");
				   add_submenu_page(__FILE__,' &raquo; Import',' &raquo; Import','activate_plugins',$_GET['page'],"_wpr_handle_request");
		}
        else
        {
                   add_submenu_page(__FILE__,'Import/Export Subscribers','Import/Export Subscribers','activate_plugins','wpresponder/importexport',"_wpr_handle_request");
        }

		add_submenu_page(__FILE__,'Run CRON','Run WPR Cron','activate_plugins',"wpresponder/runcronnow.php","wpr_runcronnow_start");
	}
	
	function wp_credits()
	{
	?>
<br />
<br />

<?php
	}
	
	
	function wpr_replace_tags($sid,&$subject,&$body,$additional = array())
	{
		global $wpdb;
	
		$query = "SELECT * FROM ".$wpdb->prefix."wpr_subscribers WHERE id='$sid'";
	
		$subscriber = $wpdb->get_results($query);
	
		$subscriber = $subscriber[0];
	
		$nid = $subscriber->nid;
	
		$query = "SELECT * FROM ".$wpdb->prefix."wpr_newsletters wehre id='$nid'";
	
		$newsletter = $wpdb->get_results($query);
	
		$newsletter = $newsletter[0];
	
		$parameters = array();
	
		//newsletter name
	
		$newsletterName = $newsletter->name;
	
		$parameters['newslettername'] = $newsletterName;
	
		$query = "SELECT * FROM ".$wpdb->prefix."wpr_custom_fields where nid='$nid'";
	
		$custom_fields = $wpdb->get_results($query);
	
		
	
		//blog name 
	
		$parameters['sitename'] = get_bloginfo("name");;
	
		//blog url
	
		$parameters['homeurl'] = get_bloginfo("home");
	
		//subscriber name
	
		$parameters['name'] = $subscriber->name;
	
		//the address of the sender (as required by can spam)
	
		$parameters['address'] = get_option("wpr_admin_address");
	
		//the email address
	
		$parameters['email'] = $subscriber->email;
	
		//admin email
	
		$parameters['adminemail'] = get_option('admin_email');
	
		
	
		$query = "select * from ".$wpdb->prefix."wpr_subscribers_$nid where id=$id;";
	
		$subscriber = $wpdb->get_results($query);
		
	
		$subscriber = $subscriber[0];
	
		
	
		//custom fields defined by the administrator
	
		foreach ($custom_fields as $custom_field)
	
		{
	
			$name = $custom_field->name;
	
			$parameters[$custom_field->name] = $subscriber->{$name};
	
		}
	
		
	
		$parameters = array_merge($parameters,$additional);
	
		
	
		foreach ($parameters as $name=>$value)
	
		{
	
			$subject = str_replace("[!$name!]",$value,$subject);
	
			$body =str_replace("[!$name!]",$value,$body);		
	
		}
	
		
	
	}
	
	/*
	
	This function creates temporary tables to simplify the process of fetching the
	
	subscribers and their custom field values from the database table.
	
	*/
	
	function wpr_make_subscriber_temptable($nid)
	
	{
	
		global $wpdb;
	
		//create the main table for the other purposes.
	
		//get a list of all custom fields and then form the 
	
		
	
		$query = "select * from ".$wpdb->prefix."wpr_custom_fields where nid=$nid";
	
		$cfields = $wpdb->get_results($query);
	
		
	
		//get the columns of the subscribers table.
		$query = "show columns from ".$wpdb->prefix."wpr_subscribers";
		$columns = $wpdb->get_results($query);
		$subsTableColumnList = array();
		foreach ($columns as $column)
	
		{
	
			$subsTableColumnList[] = $column->Field;
	
		}
	
		
	
		$count = count($cfields);
	
		$finaltable = $count;
	
		$size = strlen(sprintf("%b",$count));
	
		$formatSpec = "%'0".$size."b";
	
		//used to specify the alias for the table in the table join to make the view.
	
		$fields = array();
	
		$tables = array();
	
		$args = array();
	
		$finaltable = sprintf($formatSpec,$finaltable);
	
		$mainTableAlias = str_replace("1","b",str_replace("0","a",$finaltable));
	
		if (count($cfields) >0)
	
		{
	
			foreach ($cfields as $num=>$cfield)
	
			{
	
				$name = $cfield->name;
	
				$number = sprintf($formatSpec,$num);
	
				//name of field
	
				$tableAlias = str_replace("1","b",str_replace("0","a",$number)); //replace 0=a , 1=b
	
				$table[$name] = $tableAlias;
	
				$fields[] = $tableAlias.".$name $name";
	
				$args[] = $tableAlias.".id=".$mainTableAlias.".id";
	
				
	
			}
	
		}
	
		$lastIndex = count($table)-1;
	
		
	
		//now to add the wp_wpr_subscribers table's columns.. i may change the structure later on.. so i do this.
	
		foreach ($subsTableColumnList as $name)
	
		{
	
			$fields[] = $mainTableAlias.".$name $name";
	
		}
	
		//the list of fields in the view.
	
		$fieldlist = implode(", ",$fields);
	
		$prefix = $wpdb->prefix;
	
		//the table names and their aliases
	
		$tablenames = array();
	
		if (count($table) > 0)
	
		{
	
			foreach ($table as $name=>$alias)
	
			{
	
				$tablenames[]  = $prefix."wpr_subscribers_".$nid."_".$name." $alias";
	
			}
	
		}
	
		
	
		$tablenames[] = $prefix."wpr_subscribers ".$mainTableAlias;
	
	
	
		if (count($tablenames) > 1)
	
			$tablenames = implode(", ",$tablenames);
	
		else
	
			$tablenames = $tablenames[0];
	
		if (count($args) > 0)
	
		{
	
			$joinsList = implode(" AND ",$args);
	
			$joiningConj = " AND ";
	
		}
	
		else
	
		{
	
			$joinsList = "";
	
			$joiningConj = "";
	
		}
	
		
	
		$joinsList .= $joiningConj.$mainTableAlias.".nid=$nid";
	
		
	
		$select = "SELECT $fieldlist FROM $tablenames WHERE $joinsList";
	
	
	
		$query = "CREATE TEMPORARY TABLE IF NOT EXISTS ".$prefix."wpr_subscribers_$nid as $select;";
	
	
	
		$wpdb->query($query);
	
	}
	
	function wpr_create_temporary_tables($nid)
	
	{
	
		global $wpdb;
	
		$wpdb->show_errors();
	
		$customFieldListQuery = "SELECT * FROM ".$wpdb->prefix."wpr_custom_fields where nid=$nid";
	
		$customFields = $wpdb->get_results($customFieldListQuery);
	
		if (count($customFields ) >0 )
	
		{
	
			foreach ($customFields as $field)
	
			{
	
				$name = $field->name;
	
				$query = "CREATE TEMPORARY TABLE IF NOT EXISTS ".$wpdb->prefix."wpr_subscribers_".$nid."_".$name." as SELECT a.sid id, a.value $name from ".$wpdb->prefix."wpr_custom_fields_values a, ".$wpdb->prefix."wpr_custom_fields b where a.nid=$nid and a.cid=b.id and b.name='$name';";
	
				$wpdb->query($query);
	
			}
	
			
	
			return true;
	
		}
	
		else
	
		{
	
			return false;
	
		}
	
	}
	
	function wpr_error($error)
	
	{
	
		global $wpdb;
	
		$query = "insert into ".$wpdb->prefix."wpr_errors (time,error,notified) values (".time().",'$error',0);";
	
		$wpdb->query($query);
	
	}
	
	function wpr_place_tags($sid,&$strings,$additional=array())
	
	{
	
		global $wpdb;
	
		$query = "SELECT * FROM ".$wpdb->prefix."wpr_subscribers WHERE id='$sid'";
	
		$subscriber = $wpdb->get_results($query);
	
		$subscriber = $subscriber[0];
	
		$nid = $subscriber->nid;
	
		$id = $subscriber->id;
	
		
	
		$query = "SELECT * FROM ".$wpdb->prefix."wpr_newsletters where id='$nid'";
	
		$newsletter = $wpdb->get_results($query);
	
		$newsletter = $newsletter[0];
	
		$parameters = array();
	
		//newsletter name
		
		
	
		$newsletterName = $newsletter->name;
	
		$parameters['newslettername'] = $newsletterName;
	
		$query = "SELECT * FROM ".$wpdb->prefix."wpr_custom_fields where nid='$nid'";
	
		$custom_fields = $wpdb->get_results($query);
	
		//blog name 
	
		$parameters['sitename'] = get_bloginfo("name");
	
		//blog url
	
		$parameters['homeurl'] = get_bloginfo("home");
	
		//subscriber name
	
		$parameters['name'] = $subscriber->name;
	
		//the address of the sender (as required by can spam)
	
		$parameters['address'] = get_option("wpr_admin_address");
	
		//the email address
	
		$parameters['email'] = $subscriber->email;
	
		//admin email
		
		$parameters['todays_date'] = date("jS, F Y");
		$parameters['todays_date_mm_dd_yyyy'] = date("m-d-Y");
	
		$parameters['adminemail'] = get_option('admin_email');
	
		//custom fields defined by the administrator
		$customFieldsOfNewsletterOfSubscriber = _wpr_newsletter_custom_fields_get($nid);
		
		$customFieldValuesTableName = $wpdb->prefix."wpr_custom_fields_values";
		
		foreach ($customFieldsOfNewsletterOfSubscriber as $customField)
		{
			$getCustomFieldValueQuery = $wpdb->prepare("SELECT `value` FROM `$customFieldValuesTableName` WHERE cid=%d AND sid=%d",$customField->id,$id);
			$value = $wpdb->get_var($getCustomFieldValueQuery);
			$parameters[$customField->name] = $value;
		}
		
		$parameters = array_merge($parameters,$additional);
		foreach ($parameters as $tag=>$value)
		{
                    foreach ($strings as $index=>$string)
                    {
                        $strings[$index] = str_replace("[!$tag!]",$value,$string);
                    }
		}
	
	}
	
	function getMailTransport()
	
	{
	
		 $isSmtpOn = (get_option("wpr_smtpenabled")==1)?true:false;
	
			//get the proper email transport to use.
	
		 if ($isSmtpOn)
	
				{
	
	
	
				$smtphostname = get_option("wpr_smtphostname");
	
				$smtpport = get_option("wpr_smtpport");
	
	
	
				$doesSmtpRequireAuth = (get_option("wpr_smtprequireauth")==1)?true:false;
	
				$isSecureSMTP = (in_array(get_option("wpr_smtpsecure"),array("ssl","tls")))?true:false;
	
				$smtpsecure = get_option("wpr_smtpsecure");
	
	
	
				$transport = Swift_SmtpTransport::newInstance();
	
	
	
				$transport->setHost($smtphostname);
	
				$transport->setPort($smtpport);
	
				if ($doesSmtpRequireAuth)
                                {
                                    $smtpusername = get_option("wpr_smtpusername");
                                    $smtppassword = get_option("wpr_smtppassword");
                                    $transport->setUsername($smtpusername);
                                    $transport->setPassword($smtppassword);
                                }

                                    if ($isSecureSMTP)
                                    {
                                        $transport->setEncryption(get_option('wpr_smtpsecure'));
                                    }
	
			}
	
			else
	
				{
	
	
	
					$transport = Swift_MailTransport::newInstance();
	
				}
	
	
	
	
	
				return $transport;
	
	}
	
	function wpr_processqueue()
	{
		global $wpdb;	
	
		
		//$hourlyLimit = get_option("wpr_hourlylimit");
		//$hourlyLimit = (int) $hourlyLimit;
	
		//$limitClause = ($hourlyLimit ==0)?"":" limit ".$hourlyLimit;
	
		$query = "SELECT * FROM ".$wpdb->prefix."wpr_queue where sent=0 LIMIT 500";	
		$results = $wpdb->get_results($query);
		foreach ($results as $mail)  	
		{
			$mail = (array) $mail;
			
			try {
				
				dispatchEmail($mail);
			}
			catch (Swift_RfcComplianceException $exception) //invalidly formatted email.
			{
				_wpr_disable_subscriber($mail['to']);
			}
			catch (Exception $excep)
			{
				//placeholder for now. should not crash the queue
			}
			$query = "UPDATE ".$wpdb->prefix."wpr_queue set sent=1 where id=".$mail['id'];
			$wpdb->query($query);
		}
	
	}
	
	function _wpr_disable_subscriber($email)
	{
		global $wpdb;
		$query = "UPDATE ".$wpdb->prefix."wpr_subscribers set active=3, confirmed=0 where email='$email'";
		$wpdb->query($query);
	}
	
	
	/*
	
	 * The function that actually sends the email
	
	 *
	
	 * Arguments : $mail = array(
	
	 *                             to = The recipient's email address
	
	 *                             from = The from email address
	
	 *                             fromname = The nice name from which the email is sent
	
	 *                             htmlbody = The html body of the email
	
	 *                             textbody = The text body of the email
	
	 *                             htmlenabled = Whether the html body of the email is enabled	

	 *                                           1 = Yes, the html body is enabled
	 *                                           0 = No, the html body is disabled.
	 *                             attachimages = Whether the images are to be attached to the email
	
	 *                                           1 = Yes, attach the images
	
	 *                                           0 = No,  don't attach the images
	
	 *
	
	 */
	
	function dispatchEmail($mail)
	{
		global $wpdb;
		$return_path = $GLOBALS['_wpr_cache']['return_path'];
		
		if (empty($return_path))
		{
			$getReturnPathQuery = "SELECT option_value FROM wp_options WHERE option_name='_wpr_network_return_path';";
			$value = $wpdb->get_var($getReturnPathQuery);
			$return_path = $value;
			$GLOBALS['_wpr_cache']['return_path'] = $return_path;
		}		
		
		$transport = getMailTransport();
		$to = $mail['to'];
		
		if (!empty($return_path)) 
		{
			$from = $return_path;
		}
		else
		{
			$from = $mail['from'];			
		}
		
		$response_email = $mail['from'];
		
		$fromname = $mail['fromname'];
		if (empty($from))
		{
			$from = get_bloginfo("admin_email");
			$fromname = get_bloginfo("name");
		}

		if (empty($response_email))
		{
			$response_email = get_bloginfo("admin_email");
		}
		
		
		
		$mailer = Swift_Mailer::newInstance($transport);
		$message = Swift_Message::newInstance($mail['subject']);
		$message->setFrom(array($from=>$fromname));
		
		$message->setTo($to);
		
		$message->setReplyTo($response_email);
		
		//add a html body only if the
		$headers = $message->getHeaders();
		
		
		if (!empty($return_path) && is_email($return_path))
			$headers->addPathHeader('Return-Path', $return_path);
			
		
		//prevention of empty emails from going out. 
		$stripped_html = $mail['htmlbody'];
		$stripped_html = trim($stripped_html);
		if (empty($mail['textbody']) && empty($stripped_html))
		{
			return;
		}
		
		if ($mail['htmlenabled'] ==1 && empty($stripped_html))
		{
			$mail['htmlenabled'] = 0;
		}


		$mail['textbody'] = stripslashes($mail['textbody']);
		
		
		if ($mail['htmlenabled']==1 && !empty($mail['htmlbody']))
		{
			
			$mail['htmlbody'] = stripslashes($mail['htmlbody']);
			if ($mail['attachimages'] == 1)
			{
				attachImagesToMessageAndSetBody($message,$mail['htmlbody']);
			}
			else
			{
				$message->setBody($mail['htmlbody'],'text/html');
			}
			$message->addPart($mail['textbody'],'text/plain');
		}
		else
		{
			$message->setBody($mail['textbody'],'text/plain');
		}	
		$mailer->batchSend($message);
	
	}
	
	function attachImagesToMessageAndSetBody(&$message,$body)
	
	{
	
		$imagesInMessage = getImagesInMessage($body);
	
	
	
		foreach ($imagesInMessage as $imageUrl)
	
		{
	
			$cid = $message->embed(Swift_Image::fromPath($imageUrl));
	
			$body = str_replace($imageUrl,$cid,$body);
	
		}
	
		$message->setBody($body,'text/html');
	
	}
	
	function getImagesInMessage($message)
	
	{
	
		$startPos = 0;
	
		$list = array();
			$message = " $message"; //if the image tag is at position 0, the loop will not even start. 
	
	
	
	
		while (strpos($message,"<img",$startPos))
		{
					
	
			$start = strpos($message,"<img",$startPos);
	
			$end = strpos($message,">",$start+4);
	
			$startPos = $end;
				 
	
				//find the src="
	
			if ($end)
	
			{
	
				$begin = strpos($message,"src=\"",$start);
	
				$end = strpos($message,"\"",$begin+5);
	
				$theURL = substr($message,$begin+5,$end-$begin-5);
	
				if ($theURL[0] == "/") //then we have a relative path. attach the blog's hostname in the beginning.
	
				{
	
					$url = str_replace("http://","",get_option("siteurl"));
	
					$url = explode("/",$url);
	
					
	
					$theURL = "http://".$url[0].$theURL;
	
				}
	
				else if (strpos($theURL,"http://") > 0) //probably a relative path to the blog root.
	
				{
	
					$theURL = get_option("siteurl")."/".$theURL;
	
				}
	
				$list[] = $theURL;
	
			}
	
			else
	
			{
	
				$startPos = $start+4; // an opening image tag without a closing '>' ? then we skip that image.
	
				continue;
	
			}
	
		}
	
		 
		return array_unique($list);
	
	}
	
	function email($to,$subject,$body)
	
	{
	
		$transport = getEmailTransport();
	
		$message = Swift_message::newInstance($subject);
	
		$message->setFrom(array(get_option("admin_email")=>get_option("blogname")));
	
		$message->setTo($to);
	
		$message->setBody($body);
	
		$message->batchSend();
	
	} 
	
	
	
	
	function wpr_cronschedules()
	{
			$schedules['every_five_minutes'] = array(
				 'interval'=> 300,
				 'display'=>  __('Every 5 Minutes')
				  );
			
			$schedules['every_ten_minutes'] = array(
				 'interval'=> 600,
				 'display'=>  __('Every 5 Minutes')
				  );
			$schedules['every_minute'] = array(
				 'interval'=> 60,
				 'display'=>  __('Every Minute')
				  );

			$schedules ['every_half_hour'] = array(
												   'interval'=>1800,
												   'display'=>__('Every Half an Hour')
												   );
			 return  $schedules;
	}
        
        add_filter('cron_schedules','wpr_cronschedules');
	function wpr_get_unsubscription_url($sid)
	{
			$baseURL = get_bloginfo("home");
			$subscriber = _wpr_subscriber_get($sid);
			$newsletter = _wpr_newsletter_get($subscriber->nid);
			$nid = $newsletter->id;
			$string = $sid."%$%".$nid."%$%".$subscriber->hash;
			$codedString = base64_encode($string);
			$unsubscriptionUrl = $baseURL."/?wpr-manage=$codedString";
			return $unsubscriptionUrl;
	}

	function sendConfirmedEmail($id)
	{
		global $wpdb;
		$query = "select * from ".$wpdb->prefix."wpr_subscribers where id=$id";
		$sub = $wpdb->get_results($query);
		$sub  = $sub[0];
		//get the confirmation email and subject from newsletter

		$newsletter = _wpr_newsletter_get($sub->nid);

		$confirmed_subject = $newsletter->confirmed_subject;

		$confirmed_body = $newsletter->confirmed_body;

		//if a registered form was used to subscribe, then override the newsletter's confirmed email.

		$sid = $sub->id; //the susbcriber id
		$unsubscriptionURL = wpr_get_unsubscription_url($sid);

		$unsubscriptionInformation = "\n\nTo manage your email subscriptions or to unsubscribe click on the URL below:\n$unsubscriptionURL\n\nIf the above URL is not a clickable link simply copy it and paste it in your web browser.";


		$fid = $args[2];
		$query = "SELECT a.* from ".$wpdb->prefix."wpr_subscription_form a, ".$wpdb->prefix."wpr_subscribers b  where a.id=b.fid and b.id=$sid;";

		$form = $wpdb->get_results($query);
		if (count($form))
		{
			 $confirmed_subject = $form[0]->confirmed_subject;
			 $confirmed_body = $form[0]->confirmed_body;
		}

		$confirmed_body .= $unsubscriptionInformation;

		$params = array($confirmed_subject,$confirmed_body);

		wpr_place_tags($sub->id,$params);

		$fromname = $newsletter->fromname;
		if (!$fromname)
		{
			$fromname = get_bloginfo('name');
		}

		$fromemail = $newsletter->fromemail;
		if (!$fromemail)
		{
			$fromemail = get_bloginfo('admin_email');
		}

		$email = $sub->email;
		$emailBody = $params[1];
		$emailSubject = $params[0];
		
		$emailSubject = apply_filters("_wpr_generic",$emailSubject);
		$emailBody = apply_filters("_wpr_generic",$emailBody);
		$fromname = apply_filters("_wpr_generic",$fromname);
		$fromemail = apply_filters("_wpr_generic",$fromemail);

		$mailToSend = array(
								'to'=>$email,
								'fromname'=>  $fromname,
								'from'=> $fromemail,
								'textbody' => $emailBody,
								'subject'=> $emailSubject,
							);
		


			try {
				dispatchEmail($mailToSend);
			}
			catch (Swift_RfcComplianceException $exception) //invalidly formatted email.
			{
				//disable all subscribers with that email.
				$email = $mailToSend['to'];
				$query = "UPDATE ".$wpdb->prefix."wpr_subscribers set active=3, confirmed=0 where email='$email'";
				$wpdb->query($query);
			}
		
	}

	function wpr_enable_tutorial()
	{


		$isItEnabled = get_option("wpr_tutorial_active");
		if (empty($isItEnabled))
		{
			//enabling the tutorial for the first time.
			add_option('wpr_tutorial_active','on');
			add_option('wpr_tutorial_activation_date',time());
			add_option('wpr_tutorial_current_index',"0");//set the index to zero.

			//schedule the cron to run once every day.
		}
		else
		{
			delete_option('wpr_tutorial_active');
			add_option('wpr_tutorial_active','on');
		}

		wp_schedule_event(time()+86400, 'daily' ,  "wpr_tutorial_cron");
	}

	function wpr_disable_tutorial()
	{
		$currentStatus = get_option("wpr_tutorial_active");
		//if the tutorial series is already off then do nothing.
		if ($currentStatus == 'off')
		{
			 return false;
		}
		//if the tutorial serise is on. then turn it off
		if ($currentStatus == "on")
		{
			delete_option('wpr_tutorial_active');
			add_option('wpr_tutorial_active','off');
		}
		//if for some reason the option is missing, then create it and then set it to off.
		if (empty($currentStatus))
		{
			add_option('wpr_tutorial_active','off');
		}
		wp_clear_scheduled_hook('wpr_tutorial_cron');
		return true;
	}

	/*
	Dispatch tutorial series to the user.
	*/

	function wpr_process_tutorial()
	{
		return;
	}

	function createNotificationEmail()
	{

		$not_email = get_option('wpr_notification_custom_email');
		if (empty($not_email))
			add_option('wpr_notification_custom_email','admin_email');
		else
			return false;
	}

	function wpr_enable_updates()
	{

		$updatesOption = get_option('wpr_updates_active');

		if (empty($updatesOption))
		{
			add_option('wpr_updates_active','on');
		}
		//set the date to current date.
		delete_option('wpr_updates_lastdate');
		add_option('wpr_updates_lastdate',time());
		//schedule the cron to run daily.
		wp_schedule_event(time()+86400,'daily','wpr_updates_cron');
	}

	function wpr_disable_updates()
	{
		delete_option('wpr_updates_active');
		add_option('wpr_updates_active','off');
		wp_clear_scheduled_hook('wpr_updates_cron');
	}

	function wpr_process_updates()
	{
		return;
		
	}//end defintion of wpr_process_updates

	function getNotificationEmailAddress()
	{
		$emailAddress = get_option('wpr_notification_custom_email');
		if (empty($emailAddress))
		{
				add_option('wpr_notification_custom_email','admin_email');
		}
		if ($emailAddress != 'admin_email')
			return $emailAddress;
		else
			return get_bloginfo('admin_email');
	}

        function wpr_widgets_init()
        {
            return register_widget("WP_Subscription_Form_Widget");
        }

        function _wpr_handle_request()
        {
            //this function calls the function that triggers the
            _wpr_route_url();

        }
        function _wpr_handle_post()
        {
            if (count($_POST)>0 && isset($_POST['wpr_form']))
            {
                $formName = $_POST['wpr_form'];
                $actionName = "_wpr_".$formName."_post";
                do_action($actionName);
            }
        }

        function _wpr_route_url()
        {
            $page = $_GET['page'];
            $parts = explode("/",$page);
            $action = $parts[1];
            $arguments= array_splice($parts,1, count($parts));
            $actionName = "_wpr_".$action."_handle";
            _wpr_set("_wpr_view",$action);
            do_action($actionName,$arguments);
            _wpr_render_view();
        }


        function _wpr_render_view()
        {
            global $wpr_globals;
            $currentDir = str_replace(basename(__FILE__),"",__FILE__);
            
            $currentView = _wpr_get("_wpr_view");
            foreach ($wpr_globals as $name=>$value)
            {
                ${$name} = $value;
            }
            $viewfile ="$currentDir/views/".$currentView.".php";
            if (is_file($viewfile))
            include $viewfile;
            foreach ($wpr_globals as $name=>$value)
            {
                unset(${$name});
            }

        }

    }
} 
