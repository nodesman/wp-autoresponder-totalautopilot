<?php
add_action("_wpr_importexport_handle","_wpr_importexport_handler");
function _wpr_importexport_handler($parameters)
{
       switch($_GET['act'])
       {
           case 'columns':
               _wpr_import_identify_columns();
               break;
           case 'finished':
               _wpr_import_finished();
               break;
           case 'processing':
               _wpr_import_processing_page();
               break;
           case 'trash':
               _wpr_import_trash();
               break;
           default:
               _wpr_import_export_home();
       }
       
    //get a list of newsletter   
}

add_action("_wpr_force_import_post","_wpr_import_force");


function _wpr_import_force() 
{
    $key = $_POST['key'];
    _wpr_import_process_rows($key, 20);
    wp_redirect("admin.php?page=wpresponder/importexport");
}

add_action("_wpr_trash_import_post","_wpr_import_trash");


function _wpr_import_trash() 
{
    global $wpdb;
    $key = $_POST['key'];
    $deleteAllQuery = sprintf("DELETE from %swpr_subscriber_import WHERE `key`='%s'",$wpdb->prefix,$key);
    $wpdb->query($deleteAllQuery);
    wp_redirect("admin.php?page=wpresponder/importexport");
}

/*
 * 
 */
function _wpr_import_processing_page()
{
    global $wpdb;
    $key = $_GET['key'];
    
    $sample = _wpr_get_import_sample($key);
    
    //settings
    $settings = unserialize(stripslashes($sample[0]->settings));
    
    //report
    $report = unserialize(stripslashes($sample[0]->report));
   
    
    $nid = $settings['newsletter'];
    $newsletter = _wpr_newsletter_get($nid);
    
    _wpr_set("newsletter_name",$newsletter->name);    
    
    $aid = (int) $settings['autoresponder'];
    if ($aid != 0)
    {
        $autoresponder = _wpr_autoresponder_get($aid);
        $autoresponder_name = $autoresponder->name;
    }
    else
    {
        $autoresponder_name= __("None"); 
    }
    _wpr_set("autoresponder",$autoresponder_name);

    $pid = (int) $settings['postseries'];
    if ($pid != 0)
    {
        $posteseries = _wpr_postseries_get($pid);
        $postseries_name = $posteseries->name;
    }
    else
    {
        $postseries_name= __("None"); 
    }
    
    $blog_subscription = trim($settings['blog_subscription']);
    if (empty($blog_subscription))
    {
        $subscription_name = __("No blo subscription");
    }
    else if ($blog_subscription == 'all')
    {
        $subscription_name = __("All posts published");
    }
    else
    {
        $catid = (int) $blog_subscription;
        if ($catid != 0)
        {
            $category = get_category($catid);
            $subscription_name = $category->name;
        }
        else
            throw new Exception("Unknown subscription type for ");
    }
    
    _wpr_set("postseries",$postseries_name);
    _wpr_set("blogsubs",$subscription_name);
    _wpr_set("report",$report);
    _wpr_set("settings",$settings);
    _wpr_set("_wpr_view","import.processing");
    
}


function _wpr_delete_abandonded_imports()
{
    global $wpdb;
    $last_run = (int) get_option("_wpr_import_empty_cron_last_run");
    
    if ($last_run ==0 || 86400 > time()-$last_run)
    {
        $time = time();
        $twentyFourHoursAgo = time()-86400;
        $deleteQuery = sprintf("DELETE FROM %swpr_subscriber_import WHERE processed_status != 0 AND `time`< %s;",$wpdb->prefix,$twentyFourHoursAgo);
        $wpdb->query($deleteQuery);
    }
    update_option("_wpr_import_empty_cron_last_run",time());
}


function _wpr_import_finished()
{
    
    $settings = unserialize(base64_decode($_GET['settings']));
    $report =unserialize(base64_decode($_GET['report']));
    
    $nid = $settings['newsletter'];
    $newsletter = _wpr_newsletter_get($nid);
    
    _wpr_set("newsletter_name",$newsletter->name);    
    
    $aid = (int) $settings['autoresponder'];
    if ($aid != 0)
    {
        $autoresponder = _wpr_autoresponder_get($aid);
        $autoresponder_name = $autoresponder->name;
    }
    else
    {
        $autoresponder_name= __("None"); 
    }
    _wpr_set("autoresponder",$autoresponder_name);

    $pid = (int) $settings['postseries'];
    if ($pid != 0)
    {
        $posteseries = _wpr_postseries_get($pid);
        $postseries_name = $posteseries->name;
    }
    else
    {
        $postseries_name= __("None"); 
    }
    
    $blog_subscription = $settings['blog_subscription'];
    if ($blog_subscription == 'all')
    {
        $subscription_name = __("All posts published");
    }
    else
    {
        $catid = (int) $blog_subscription;
        if ($catid != 0)
        {
            $category = get_category($catid);
            $subscription_name = $category->name;
        }   
        else
            throw new Exception("Unknown subscription type for ");
    }
    
    _wpr_set("postseries",$postseries_name);
    _wpr_set("blogsubs",$subscription_name);
    _wpr_set("report",$report);
    _wpr_set("settings",$settings);
    _wpr_set("_wpr_view","import.finished");
}

function _wpr_get_import_sample($key)
{
    global $wpdb;
    $getSettingsQuery = $wpdb->prepare("SELECT * FROM  ".$wpdb->prefix."wpr_subscriber_import WHERE `key`=%s LIMIT 1",$key);
    $results = $wpdb->get_results($getSettingsQuery);
    return $results;
}


function _wpr_import_identify_columns()
{
    global $wpdb;
    $key = trim(addslashes($_GET['key']));
    //get the import settings    
    $results = _wpr_get_import_sample($key);
    if (0==count($results))
    {
       $errors[] = __("Invalid import key. No such CSV file found in database");
    }
    else
    {
        $settings = unserialize(stripslashes($results[0]->settings));
        $nid = $settings['newsletter'];
        $content = unserialize(stripslashes($results[0]->content));
        $getCustomFielsQuery = $wpdb->prepare("SELECT * FROM ".$wpdb->prefix."wpr_custom_fields WHERE nid=%d",$nid);
        $customFieldsList = $wpdb->get_results($getCustomFielsQuery);
        $columnNames = array(
            '-1'=>__("Name"),
            '0' => __("E-Mail Address")
        );
        //get the corresponding custom fields
        foreach ($customFieldsList as $field)
        {
            $columnNames[$field->id]=$field->label;
        }
    }
    if (isset($content))
    {
        _wpr_set("list",$content);
        _wpr_set("columns",$columnNames);
    }
    else
    {
        _wpr_set("show_error",true);
    }
    if (!_wpr_get("errors"))
        _wpr_set("errors",$errors);    
    _wpr_set("_wpr_view","import.fifthstep");    
    //set the correct view for this page. 
}

add_action("_wpr_wpr_subscriber_export_post","_wpr_export");

function _wpr_export()
{
	$nid = $_POST['newsletter'];
	export_csv($nid);
	exit;
}

function _wpr_import_export_home()
{
	global $wpdb;
         $newsletters = _wpr_newsletters_get();
	
	//get a list of autoresponder-newsletter relations
	$autoresponderList = array();
	foreach ($newsletters as $newsletter)
	{
		$getAutorespondersQuery = $wpdb->prepare("SELECT * FROM ".$wpdb->prefix."wpr_autoresponders WHERE nid=%d", $newsletter->id);
		$results = $wpdb->get_results($getAutorespondersQuery);
		$autoresponderList[$newsletter->id] = $results;
	}
	
	//get a list of categories in the blog
	
	 $args = array(
		'type'                     => 'post',
		'child_of'                 => 0,
		'orderby'                  => 'name',
		'order'                    => 'ASC',
		'hide_empty'               => 0,
		'hierarchical'             => 0,
		'taxonomy'                 => 'category',
		'pad_counts'               => false );
	$categories = get_categories($args);
	$getPostSeriesQuery = $wpdb->prepare("SELECT * FROM ".$wpdb->prefix."wpr_blog_series;");
	$results = $wpdb->get_results($getPostSeriesQuery);
        
        $getPendingJobsQuery = sprintf("SELECT DISTINCT `key` FROM ".$wpdb->prefix."wpr_subscriber_import WHERE processed_status=0 AND column_index!='';");
        $pendingJobs = $wpdb->get_results($getPendingJobsQuery);
        
        foreach ($pendingJobs as $job)
        {
            //how many rows pending?
            $getPendingQuery = sprintf("SELECT COUNT(*) pending_count FROM ".$wpdb->prefix."wpr_subscriber_import WHERE `key`='%s' AND processed_status=0",$job->key);
            $pending = $wpdb->get_results($getPendingQuery);
            $count = $pending[0]->pending_count;
            
            //how many rows finished?
            $getFinishedQuery = sprintf("SELECT COUNT(*) finished_count FROM ".$wpdb->prefix."wpr_subscriber_import WHERE `key`='%s' AND processed_status=1",$job->key);
            $finished = $wpdb->get_results($getFinishedQuery);
            $finished_count = $finished[0]->finished_count;
            
            //what are the settings?
            $getSettingsQuery = sprintf("SELECT * FROM ".$wpdb->prefix."wpr_subscriber_import WHERE `key`='%s' ORDER BY `id` DESC LIMIT 1",$job->key);
            $settingsRes = $wpdb->get_results($getSettingsQuery);
            $settings_str = $settingsRes[0]->settings;
            $settings = unserialize($settings_str);
            
                        
            $subscriberInLastRow = unserialize(stripslashes($settingsRes[0]->content));
            $numberOfSubscribersInLastRow = count($subscriberInLastRow);
            
            
            if ($count !=0) { //if there are pending subscribers to import
                $actual_finished=$finished_count*50;
                $actual_pending = ($count-1)*50;
                $actual_pending += $numberOfSubscribersInLastRow;
            }
            else
            {
                $actual_finished = ($finished_count-1)*50;
                $actual_finished+= $numberOfSubscribersInLastRow;
                $actual_pending=0;
            }
            $sample = _wpr_get_import_sample($job->key);
            $report = unserialize(stripslashes($sample[0]->report));
            $jobs[$job->key] = array(
                                        "pending"=> $actual_pending, 
                                        "finished"=>$actual_finished,
                                        "settings"=> import_string($settings),
                                        "report"=> $report
                                    );
        }

	//get a list of post series in teh blog
        _wpr_set("newslettersList",$newsletters);
	_wpr_set("autoresponderNewsletterList",$autoresponderList);
	_wpr_set("postseriesList",$results);
	_wpr_set("categories",$categories);
	_wpr_set("autoresponder_newsletter_relation",$autoresponderList);
        _wpr_set("pending_import_jobs",$jobs);
}

add_action("_wpr_wpr_import_first_post","_wpr_import_first_post");
add_action("_wpr_wpr_import_followup_post","_wpr_import_followup_post");



function _wpr_import_first_post() {
	
	global $wpdb;
	//validate
		$preload_values = array();
		$error = array();
		//check if the file has uploaded correctly
		
			
		//check if a newsletter has been selected and it exists
		$nid = (int)$_POST['nid'];
		if ($nid ==0)
		    $error[] = __("A newsletter wasn't selected. Please select a newsletter.");
		else
		{
			$checkWhetherNewsletterExistsQuery = $wpdb->prepare("SELECT * FROM ".$wpdb->prefix."wpr_newsletters WHERE id=%d",$nid);
			$results = $wpdb->get_results($checkWhetherNewsletterExistsQuery);
                        if (0 == count($results))
                            $error[] = __("You've selected a newsletter that has been deleted.");
		}
		//check if the autoresponder, if selected, exists and belongs to the selected newsletter.
		$aid = intval($_POST['aid']);
		if ($aid != 0 && count($error) ==0 )
		{
			$checkWhetherAutoresponderExistsQuery = $wpdb->prepare("SELECT * FROM ".$wpdb->prefix."wpr_autoresponders WHERE nid=%d AND id=%d", $nid, $aid);
			$results = $wpdb->get_results($checkWhetherAutoresponderExistsQuery);
			if (count($results) == 0)
			{
				$error[] = __("You've selected an autoresponder that belongs to a different newsletter");
			}
		}
		
		//check if the post series exists
		$pid = (int) $_POST['pid'];
		
		if ($pid != 0 )
		{
			$checkWhetherPostSeriesExistsQuery = $wpdb->prepare("SELECT * FROM ".$wpdb->prefix."wpr_blog_series WHERE id=%d", $pid);
			$results = $wpdb->get_results($checkWhetherPostSeriesExistsQuery);
			if (0 == count($results))
			{
				$error[] = __("The post series you've selected doesn't exist or has been deleted.");
			}
		}
		
		//check if the blog category if selected, exists
		$subscription = $_POST['blog'];
                $catid = intval($subscription);
		if ($subscription != "all" && 0 != $catid)
		{
			$subscription = intval($subscription);
			$category = get_category($subscription);
			if (is_a($category, "WP_Error"))
			{
			   $error[] = __("The category that you are trying to subscribe to does not exist or has been deleted");
			}
		}
		
		
		
		//check the uploaded file
		if ($_FILES['csv']['error']!=UPLOAD_ERR_OK) {
			switch ($_FILES['csv']['error'])
			{
				case UPLOAD_ERR_INI_SIZE:
					$error[] = __("An error occured when attempting to import the file: The file is too big for the server. The maximum allowed file size is  ".ini_get('upload_max_filesize')."B");
					break;
				case UPLOAD_ERR_PARTIAL:
					$error[] = __("The file could not be imported because it was only partially uploaded");
					break;
				case UPLOAD_ERR_FORM_SIZE:
					$error[] = __("The file could not be processed because it is too big. Please upload a file less than 1.5 MB. If your file is bigger, please split them up into parts");
					break;
				case UPLOAD_ERR_NO_FILE:
					$error[] = __("No file was uploaded for importing. Please upload a file");
					break;
				case UPLOAD_ERR_NO_TMP_DIR:
					$error[] = __("The file failed to upload because it couldn't be stored on the server. The temporary folder where uploaded files are stored wasn't found on the server. This is a server misconfiguration. Please contact your administrator");
					break;
				case UPLOAD_ERR_CANT_WRITE:
					$error[] = __("The file failed to upload because it couldn't be stored on the server. The file could not be written to the disk. This is a server misconfiguration. Please contact your administrator");
					break;
				case UPLOAD_ERR_EXTENSION:
					$error[] = __("The file failed to upload because it couldn't be stored on the server. A PHP extension prevented the file from being uploaded. Please contact your administrator.");
					break;
					
			}
		}
                else
                {
                    ini_set("auto_detect_line_endings",true);
                    $fp = fopen($_FILES['csv']['tmp_name'],"r");
                    $row = fgetcsv($fp);

                    if (!is_array($row))
                            $error[] = __("The file you uploaded is not a CSV file");

                    fclose($fp);
                }
	//load into database
	               
	if (count($error) == 0) 
	{
		//form the settings array
		$settings = array(
                                    "newsletter"=>$nid,
                                    "autoresponder"=>$aid,
                                    "postseries"=>$pid,
                                    "blog_subscription"=>$subscription,
                             );
							
		//load the file into the database
		$filename = $_FILES['csv']['name'];
		$fp = fopen($_FILES['csv']['tmp_name'],"r");
		$key = md5($_FILES['csv']['tmp_name'].microtime());
		$importtime = time();
		while (!feof($fp))
		{
			//read 50 lines
			$rows = array();
                        $whetherReadErrorOccuredFlag = false;
                        
			for ($iter=0;$iter<50;$iter++)
			{
				$row = fgetcsv($fp);
				//handle error
                                
				if (!is_array($row) && !feof($fp))
				{
					$deleteAlreadyImportedRowsQuery = $wpdb->prepare("DELETE FROM ".$wpdb->prefix."wpr_subscriber_import WHERE key=%s",$key);
					$wpdb->query($deleteAlreadyImportedRowsQuery);
					//delete the imported rows
					
					$error[] = __("The could not be fully read as it had some errors. To protect the database integrity the file has not been imported");
					break;
				}
				if (feof($fp))
					break;
				$rows[]= $row;
			}
                        if (!$whetherReadErrorOccuredFlag)
                        {
                            $content = addslashes(serialize($rows));
                            $addToDatabaseQuery = $wpdb->prepare("INSERT INTO ".$wpdb->prefix."wpr_subscriber_import (`content`, `filename`, `key`, `time`, `settings`) VALUES (%s,%s,%s,%s,%s);",$content,$filename, $key, $importtime ,serialize($settings));
                            $wpdb->query($addToDatabaseQuery);
                        }
		}
		//load it into the database
	}
	//set 100 rows into the view variable
	
	if (0 == count($error))
	{
		?>
		<script>window.location='admin.php?page=wpresponder/importexport&act=columns&key=<?php echo $key ?>';</script>
		<?php
		exit;
	}
	else
	{
		_wpr_set("importerrors",$error);
	}
}

function import_string($settings)
{
     $nid = $settings['newsletter'];
    $newsletter = _wpr_newsletter_get($nid);
    
    $String[] = __("Newsletter '{$newsletter->name}'");
    
    $aid = (int) $settings['autoresponder'];
    if ($aid != 0)
    {
        $autoresponder = _wpr_autoresponder_get($aid);
        $autoresponder_name = $autoresponder->name;
        $String[] = __("Autoresponder '{$autoresponder_name}'");
    }
    
    

    $pid = (int) $settings['postseries'];
    if ($pid != 0)
    {
        $posteseries = _wpr_postseries_get($pid);
        $postseries_name = $posteseries->name;
        $String[] = __("Post series '$postseries_name'");
    }
    
    
    $blog_subscription = $settings['blog_subscription'];
    if (empty($blog_subscription))
    {
        $subscription_name = __("No blog subscription");
    }
    if ($blog_subscription == 'all')
    {
        $subscription_name = __("All blog posts published");
    }
    else
    {
        $catid = (int) $blog_subscription;
        if ($catid != 0)
        {
            $category = get_category($catid);
            $subscription_name = __("Posts in '{$category->name}' category");   
        }   
    }
    $String[] = $subscription_name;
    
    $settings_string = implode(" ,",$String);
    
    return $settings_string;
    
}




$import_report = array();

function _wpr_wpr_import_finish_post()
{
	//start importing.
	global $wpdb;
        global $import_report;
        set_time_limit(300);
        $key = $_GET['key'];
        $key = addslashes($key);
        $key = trim($key);
        $results = _wpr_get_import_sample($key);
        $import_results = array();
        //validate the inputs
        $columns = array();
        $fieldsSpecified = array();
        
        foreach ($_POST as $name=>$value)
        {
            if (!preg_match("/column_[0-9]+/",$name))
                    continue;
            if ($value=='')
                continue;
            if (in_array($value,$fieldsSpecified))            
            {
                $error[] = __("A column has been specified more than once.");
                break;
            }
            $fieldsSpecified[] = $value;
            $cidOfIndex[$value] = str_replace("column_","",$name);
        }
        
        if ( !in_array(-1, $fieldsSpecified) || !in_array(0,$fieldsSpecified) )
        {
            $error[] = __("The name and/or email field was not selected among the columns. ");
        }
        
        
        if (0 == count($error))
        {
            //set the settings array to store the cidOfIndex array
            $columnIndexString = serialize($cidOfIndex);
            $columnIndexString = addslashes($columnIndexString);
            $setColumnIndexForRowsQuery = sprintf("UPDATE %swpr_subscriber_import SET `column_index`='%s' WHERE `key`='%s'",$wpdb->prefix,$columnIndexString, $key);
            
            $wpdb->query($setColumnIndexForRowsQuery);
            
            //get the number of rows that are part of this job
     
            $getNumberOfRowsQuery = $wpdb->prepare("SELECT count(*) number FROM ".$wpdb->prefix."wpr_subscriber_import WHERE `key`=%s;",$key);
            $numberOfRowsResult= $wpdb->get_results($getNumberOfRowsQuery);
            $numberOfRows = $numberOfRowsResult[0]->number;
            
            $finsihedAlready = false;
            
            
            _wpr_import_process_rows($key, 10); 
            
            $results = _wpr_get_import_sample($key);
            $settings = unserialize(stripslashes($results[0]->settings));
            $newsletter = $settings['newsletter'];
            
            $fields = _wpr_newsletter_custom_fields_get($newsletter);
            
            /*$fieldsMap = array();
            foreach ($fields as $field)
            {
                $fieldMap["$field->id"] = $field;
            }*/
            //load one at a time.
            
           /* 
            $deleteImportedFile = $wpdb->prepare("DELETE FROM ".$wpdb->prefix."wpr_subscriber_import WHERE `key`=%s",$key);
            $wpdb->query($deleteImportedFile);
            */
            $report = base64_encode(serialize($import_report));
            $settings_serial = base64_encode(serialize($settings));
            if ($finsihedAlready)
            {
                ?>
<script>window.location='admin.php?page=wpresponder/importexport&act=finished&report=<?php echo $report ?>&settings=<?php echo $settings_serial ?>"';</script>
	    <?php
            }
            else
            {
                wp_schedule_event(time(), 'every_minute', '_wpr_import_cron',array($key,20));
            ?>
<script>window.location='admin.php?page=wpresponder/importexport&act=processing&key=<?php echo $key ?>';</script>
	    <?php
            }
	    exit;
        }
        else
        {
            //there were errors in the input sent by the user
            _wpr_set("errors",$error);
        }
}

function _wpr_import_process_rows($key, $numberOfRows)
{
    global $wpdb;
    global $import_report;
    
    //get sample
    $sample = _wpr_get_import_sample($key);
    $import_report = unserialize(stripslashes($sample[0]->report));

    
    for ($iter=0;$iter<$numberOfRows;$iter++)
    {

        $getARowQuery = $wpdb->prepare("SELECT * FROM ".$wpdb->prefix."wpr_subscriber_import WHERE `key`=%s AND processed_status=0 AND column_index IS NOT NULL LIMIT 1",$key);

        $oneRowQuery = $wpdb->get_results($getARowQuery);
        $row = $oneRowQuery[0];
        $cidOfIndex = unserialize(stripslashes($row->column_index));
        $filerows = unserialize(stripslashes($row->content));
        $settings = unserialize(stripslashes($row->settings));
        foreach ($filerows as $row)
        {

            $subscriber = array();
            $custom_fields = array();
            foreach ($cidOfIndex as $cid=>$index)
            {
                $cid = (int) $cid;
                switch ($cid)
                {
                    case -1:
                        $subscriber['name'] = $row[$index];
                        break;
                    case 0:
                        $subscriber['email'] = $row[$index];
                        break;
                    default:
                        $custom_fields["$cid"] = $row[$index];
                }
            }
            _wpr_import_subscriber($subscriber,$custom_fields,$settings);
        }
        $updateCurrentSectionProcessedQuery = $wpdb->prepare("UPDATE {$wpdb->prefix}wpr_subscriber_import SET processed_status=1 WHERE id=%d",$oneRowQuery[0]->id);
        $wpdb->query($updateCurrentSectionProcessedQuery);
    }
    $importStr = addslashes(serialize($import_report));
    $updateReportQuery = sprintf("UPDATE %swpr_subscriber_import SET report='%s' WHERE `key`='%s'",$wpdb->prefix,$importStr,$key);
    $wpdb->query($updateReportQuery);

    //check if the job finished
    $checkWhetherAnyPendingQuery = sprintf("SELECT COUNT(*) num FROM %swpr_subscriber_import WHERE processed_status=0 AND `key`='%s';",$wpdb->prefix, $key);
    
    $numRes = $wpdb->get_results($checkWhetherAnyPendingQuery);
    $num = $numRes[0]->num;
    
    if ($num ==0)
    {
        $args = array($key,20);
        $timestamp = wp_next_scheduled("_wpr_import_cron", $args);
        wp_unschedule_event($timestamp,"_wpr_import_cron", $args);
    }
}

add_action("_wpr_import_cron",  "_wpr_import_process_rows",1,2);
add_action("_wpr_wpr_import_finish_post","_wpr_wpr_import_finish_post");
 
function _wpr_import_subscriber($subscriber, $custom_fields,$settings)
{
    global $wpdb;
    global $import_report;
    //check if the subscriber with that informatino exists in that newsletter
    $nid = $settings['newsletter'];
    $emailAddress = $subscriber['email'];
    
    if (!is_email($emailAddress))
    {
        $import_report["Invalid"]++;
        return false;
    }
    
    $checkWhetherSubscriberExistsQuery = $wpdb->prepare("SELECT * FROM ".$wpdb->prefix."wpr_subscribers WHERE nid=%d AND email=%s",$nid,$emailAddress);
    $results = $wpdb->get_results($checkWhetherSubscriberExistsQuery);
    $whetherSubscribersExists = (0 < count($results));
    $whetherSubscriberSubscribedAndActive = ($results[0]->active==1 && $results[0]->confirmed==1);
    
    //if he does then update him with the new information
    $currentTime  = time();
    
    if ($whetherSubscribersExists)
    {
        $activateAndUpdateTheSubscriptionQuery = $wpdb->prepare("UPDATE ".$wpdb->prefix."wpr_subscribers SET active=1, confirmed=1, name=%s, date=%s WHERE email=%s AND nid=%d;",$subscriber['name'], $currentTime, $emailAddress, $nid);
        $wpdb->query($activateAndUpdateTheSubscriptionQuery);
        $import_report["Updated"]++;
        //get the subscriber information
    }
    else
    {
        for ($iter=0;$iter<8;$iter++)
        {
            $number = rand(98,122);
            $char = chr($number);
            $string .= $char;
        }
        
        $insertSubscriberToDatabaseQuery = $wpdb->prepare("INSERT INTO ".$wpdb->prefix."wpr_subscribers (nid, name, email, fid, active, confirmed, hash, date)
            VALUES (%d,%s,%s,0,1,1,%s,'%s');
        ",$nid,$subscriber['name'],$subscriber['email'],$string,$currentTime);
        $wpdb->get_results($insertSubscriberToDatabaseQuery);
        $import_report["Imported"]++;
    }
    $getSubscriberQuery = $wpdb->prepare("SELECT * from ".$wpdb->prefix."wpr_subscribers WHERE email=%s AND nid=%d;",$emailAddress,$nid);
    $results = $wpdb->get_results($getSubscriberQuery);
    $subscriber = $results[0];
    
    $sid = $subscriber->id;
    //inserting the custom field information for this subscriber. 
    
    foreach ($custom_fields as $cid=>$value)
    {
         //ensure that this custom field exists
        $deleteExistingValueQuery = $wpdb->prepare("DELETE FROM ".$wpdb->prefix."wpr_custom_fields_values WHERE sid=%d AND cid=%d",$cid,$sid);
        $wpdb->query($deleteExistingValueQuery);
        $insertCustomFieldValueQuery = $wpdb->prepare("INSERT INTO ".$wpdb->prefix."wpr_custom_fields_values (nid, sid, cid, value)  VALUES (%d,%d,%d,%s);",$nid,$sid,$cid, $value);
        $wpdb->query($insertCustomFieldValueQuery);
    }    
    
    //autoresponder subscription
    $aid = (int) $settings['autoresponder'];
    if ($aid != 0)
    {
        $deleteExistingSubscriptionToAutoresponderQuery = $wpdb->prepare("DELETE FROM ".$wpdb->prefix."wpr_followup_subscriptions  WHERE eid=%d AND type='autoresponder' AND sid=%d",$aid,$sid);
        $wpdb->query($deleteExistingSubscriptionToAutoresponderQuery);

        //subscribe to autoresponder query
        $addAutoresponderSubscriptionQuery = $wpdb->prepare("INSERT INTO ".$wpdb->prefix."wpr_followup_subscriptions (sid, eid, type, sequence, last_date, doc) VALUES (%s,%s,'autoresponder',-1,0,%s);",$sid,$aid, $currentTime);
        $wpdb->query($addAutoresponderSubscriptionQuery);
    }
    
    //post series subscription
    $pid = (int) $settings['postseries'];
    if ($pid != 0)
    {
        $deleteExistingSubscriptionToAutoresponderQuery = $wpdb->prepare("DELETE FROM ".$wpdb->prefix."wpr_followup_subscriptions  WHERE eid=%d AND type='postseries' AND sid=%d",$pid,$sid);
        $wpdb->query($deleteExistingSubscriptionToAutoresponderQuery);  

        $addPostSeriesSubscriptionQuery = $wpdb->prepare("INSERT INTO ".$wpdb->prefix."wpr_followup_subscriptions (sid, eid, type, sequence, last_date, doc) VALUES (%d, %d, 'postseries',-1, 0, %s);", $sid,$pid, $currentTime);
        $wpdb->query($addPostSeriesSubscriptionQuery);
    }
    
    //blog subscription
    $blog_subscription_type = ($settings['blog_subscription'] == 'all')?'all':"cat";
    $cat_id = intval($settings['blog_subscription']);
    
    $insertNewBlogSubscriptionQuery = $wpdb->prepare("INSERT INTO ".$wpdb->prefix."wpr_blog_subscription (sid, type, catid) VALUES (%s,%s,%s);",$sid,$blog_subscription_type,$cat_id);
    $wpdb->query($insertNewBlogSubscriptionQuery);
    
}
/*
function _wpr_import_first_post()
{
	@session_start();		
	$newsletter= trim($_POST['newsletter']);
	$_SESSION['wpr_import_newsletter']=$newsletter;
	wp_redirect("admin.php?page=wpresponder/importexport/import/step2");
}

function _wpr_import_followup_post()
{
    @session_start();
    $_SESSION['wpr_import_followup'] = $_POST['followup'];
    wp_redirect("admin.php?page=wpresponder/importexport/import/step3");
}
add_action("_wpr_wpr_import_blogsub_post","_wpr_import_blogsub_post");
add_action("_wpr_wpr_import_upload_post","_wpr_import_upload");
function _wpr_import_blogsub_post()
{
    @session_start();
    $_SESSION['_wpr_import_blogsub'] = $_POST['blogsubscription'];
    wp_redirect("admin.php?page=wpresponder/importexport/import/step4");
}



function _wpr_import_fourth_step()
{
    _wpr_set("_wpr_view","import.fourthstep");
}

function _wpr_import_upload()
{
    @session_start();
    
}

function _wpr_import_fifth_step()
{
    $csv = $_SESSION['_wpr_csv_file'];
    $count=0;	
	$sample = array_slice($csv,0,100);
	$csv = splitToArray($sample);

    $customFields = _wpr_newsletter_all_custom_fields_get($_SESSION['wpr_import_newsletter']);

    $columnsRequired = array('name'=>'Name',
        'email'=>'E-Mail Address');

    foreach ($customFields as $field)
    {
        $columnsRequired[$field->name] = $field->label;
    }
    _wpr_set("list",$csv);
    _wpr_set("columns",$columnsRequired);
    _wpr_set("_wpr_view","import.fifthstep");
}



 */
