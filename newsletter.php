<?php

include "newsletter.lib.php";

include "custom_fields.php";





function _wpr_newsletter_edit()

{

	

	$id = $_GET['nid'];

	$newsletter = _wpr_newsletter_get($id);	

	if ($_POST['name'])

	{

		

		if ($_POST['name'])

		{

			$info['id'] = $_POST['id'];

			$info['name'] = $_POST['name'];

			$info['reply_to'] = $_POST['reply_to'];

			$info['description'] = $_POST['description'];

			$info['confirm_subject'] = $_POST['confirm_subject'];

			$info['confirm_body'] = $_POST['confirm_body'];

			$info['confirmed_subject'] = $_POST['confirmed_subject'];

			$info['confirmed_body'] = $_POST['confirmed_body'];

                        $info['fromname'] = $_POST['fromname'];

                        $info['fromemail'] = $_POST['fromemail'];

			_wpr_newsletter_update($info);

			?>

            <script>window.location='admin.php?page=wpresponder/newsletter.php'</script>

			<?php

            exit;

		}

		else
		{

			$error = "Name field is required";

			foreach ($_POST as $name=>$value)

			{

				$information->{$name} = $value;

			}

		}

		

	}

	_wpr_newsletter_form($newsletter,"Edit Email System","Update",$error);

	

}


function checkIfValidNewsletterName($name)
{
	$name = trim($name);
	if ( empty($name) )
		return false;
	global $wpdb;
	
	$query = "SELECT COUNT(*) num FROM ".$wpdb->prefix."wpr_newsletters where name='$name'";
	$results = $wpdb->get_results($query);
	return ($results[0]->num ==0); //if the number of newsletters with this name is zero then the name is unique
	
}


function _wpr_newsletter_add()

{

	if ($_POST['name'])
	{
		if (checkIfValidNewsletterName($_POST['name']) )
		{
			$info['name'] = $_POST['name'];
			$info['description'] = $_POST['description'];
			$info['confirm_subject'] = $_POST['confirm_subject'];
			$info['confirm_body'] = $_POST['confirm_body'];
			$info['confirmed_subject'] = $_POST['confirmed_subject'];
			$info['confirmed_body'] = $_POST['confirmed_body'];
            $info['fromname'] = $_POST['fromname'];
            $info['fromemail'] = $_POST['fromemail'];
			
			_wpr_newsletter_create($info);
			
			?>

			<script>window.location='admin.php?page=wpresponder/newsletter.php'</script>
			<?php

            exit;

		}

		else

		{

			if (empty($_POST['name']))			
			{
				$error = "Name field is required";
				
			}
			else 
			{
				$error = "The name field is not unique.";
			}

			$information = (object) $_POST;
		}

		

	}

	_wpr_newsletter_form($information,"Create Email System","Create Email System",$error);

	

}



function _wpr_newsletter_home()

{

	?>

    <div class="wrap"><h2>Email Systems</h2></div>

    <?php _wpr_newsletter_list(); ?>

    <input type="button" onclick="window.location='<?php echo $_SERVER['REQUEST_URI']; ?>&act=add'" value="Create Email System" class="button" />    

    <?php

}



function _wpr_newsletter_list()

{

	global $wpdb;

	$query = "SELECT * FROM ".$wpdb->prefix."wpr_newsletters";

	$results = $wpdb->get_results($query);

	?>

    <style>

	form {

		display:inline;

	}

	</style>

    <table class="widefat">

    <tr>

    <thead>

      <th scope="col">Name</th>

      <th scope="col">Reply-To</th>

      <th scope="col" width="600">Actions</th>

     </thead>

     </tr>

     <?php foreach ($results as $list) { ?>

     <tr>

       <td><?php echo $list->name ?></td>

       <td><?php echo $list->reply_to ?></td>

       <td>

       <input type="button" name="Edit" onclick="window.location='admin.php?page=wpresponder/newsletter.php&act=edit&nid=<?php echo $list->id ?>';" value="Edit" class="button" />


       <input type="button" name="manage" value="Manage Leads" class="button" onclick="window.location='admin.php?page=wpresponder/subscribers.php&action=nmanage&nid=<?php echo $list->id ?>';" />
       <input type="button" name="Delete" value="Delete" class="button" onclick="window.location='admin.php?page=wpresponder/newsletter.php&act=delete&nid=<?php echo $list->id ?>';" />

       <input type="button" name="E-mails" value="Custom Fields" onclick="window.location='admin.php?page=wpresponder/newsletter.php&act=custom_field&nid=<?php echo $list->id ?>';" class="button"/>

       </td>

       </tr>

       <?php

	 }	

?></table>

<?php

}



function wpr_newsletter()

{

	$action = $_GET['act'];

	

	switch ($action)

	{

		case 'add':

			_wpr_newsletter_add();

			break;

		case 'edit':

		    _wpr_newsletter_edit();

			break;

		case 'delete':
		
		   _wpr_newsletter_delete();
		   
		   break;

		case 'custom_field':

		   _wpr_newsletter_custom_fields();

		   break;
	
	
		break;

		case 'forms':

		   _wpr_newsletter_forms();

		 default:

		 _wpr_newsletter_home();

	}		

}

function _wpr_newsletter_delete()
{
	global $wpdb;
	$prefix = $wpdb->prefix;
	
	$nid = intval($_GET['nid']);
	
		if (isset($_GET['done']) && $_GET['done'] == 1)
	{
		?>
        
        <div class="wrap">
        <div class="wrap">
		<h2>Newsletter Has Been Deleted</h2>
		<br />
		The newsletter and all of the associated subscribers, subscribers' custom field data, autoresponders, autoresponder subscriptions, autoresponder messages, blog subscriptions have been deleted.
		<br />
		<br />
		<a style="margin:10px;margin-top: 20px" class="button-primary" href="admin.php?page=wpresponder/newsletter.php">&laquo; Back</a>
        </div>
        <?php
		return;
	}
	
	$checkWhetherNewsletterExistsQuery = "SELECT * FROM {$prefix}wpr_newsletters WHERE id=$nid";
	$newsletters = $wpdb->get_results($checkWhetherNewsletterExistsQuery);
	
	
	if (0 == count($newsletters))
	{
		?>
		<div class="wrap">
		<h2>Newsletter Not Found</h2>
		<br />
		The newsletter you are trying to delete does not exist.
		<br />
		<a style="margin:10px" href="admin.php?page=wpresponder/newsletter.php">&laquo; Back</a>
		<?php
		return;
	}
	
	$nid = $newsletters[0]->id;
	
	if (isset($_GET['confirm']) && $_GET['confirm'] == true && $nid != 0)
	{
	
		//delete the newsletter subscribers' subscriptions
		
		$deleteFollowupSubscriptionsQuery = "DELETE FROM `{$prefix}wpr_followup_subscriptions` WHERE sid=(SELECT id FROM {$prefix}wpr_subscribers WHERE nid=$nid);";
		$wpdb->query($deleteFollowupSubscriptionsQuery);
		
		$deleteFollowupSubscriptionsQuery = "DELETE FROM `{$prefix}wpr_blog_subscription` WHERE sid=(SELECT id FROM {$prefix}wpr_subscribers WHERE nid=$nid);";
		$wpdb->query($deleteFollowupSubscriptionsQuery);
		
		$deleteSubscriptionFormsQuery = "DELETE FROM {$prefix}wpr_subscription_form WHERE nid=$nid;";
		$wpdb->query($deleteSubscriptionFormsQuery);
		
		$deleteAutoresponderMessagesQuery = "DELETE FROM {$prefix}wpr_autoresponder_messages WHERE aid=(SELECT id FROM {$prefix}wpr_autoresponders WHERE nid=$nid);";
		$wpdb->query($deleteAutoresponderMessagesQuery);
		
		$deleteCustomFieldDataDeletionQuery = "DELETE FROM {$prefix}wpr_custom_fields_values WHERE cid=(SELECT id FROM {$prefix}wpr_custom_fields WHERE nid=$nid);";
		$wpdb->query($deleteCustomFieldDataDeletionQuery);
		
		$deleteCustomFieldsQuery = "SELECT id FROM {$prefix}wpr_custom_fields WHERE nid=$nid";
		$wpdb->query(deleteCustomFieldsQuery);
		
		$deleteSubscribersQuery = "DELETE FROM {$prefix}wpr_subscribers WHERE nid=$nid;";
		$wpdb->query($deleteSubscribersQuery);
		
		$deleteAutorespondersQuery = "DELETE FROM ".$prefix."wpr_autoresponders WHERE nid=$nid;";
		$wpdb->query($deleteAutorespondersQuery);
		
		$deleteNewsletterQuery = "DELETE FROM ".$prefix."wpr_newsletters where id=$nid";
		$wpdb->query($deleteNewsletterQuery);
		
		?>
        <script>
		window.location='admin.php?page=wpresponder/newsletter.php&act=delete&done=1';
		</script>
        <?php
		exit;
	}
	

	$newsletter_name = $newsletters[0]->name;
	?>
<div class="wrap">
   <h2>Are you sure you want to delete newsletter '<?php echo $newsletter_name ?>' ?</h2>
   
   <p>The following will also be deleted:</p>
   <p>
   <ol>
       <li>The associated susbcribers</li>
	   <li>The associated autoresponders</li>	  
		<li>All subscription forms that subscribe to this newsletter</li>
	   <li>The subscribers of this newsletter and all of their followup and blog subscriptions</li>
   </ol>
   <a class="button-primary" href="admin.php?page=wpresponder/newsletter.php&act=delete&confirm=true&nid=<?php echo $newsletters[0]->id ?>" style="margin: 10px">Yes</a> 
   <a class="button" href="admin.php?page=wpresponder/newsletter.php">No</a>
   </p>
</div>
	<?php
	
	
}


