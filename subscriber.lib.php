<?php

function _wpr_subscriber_get($id)
{
	global $wpdb;
	$query = "SELECT * FROM ".$wpdb->prefix."wpr_subscribers where id=$id";
	$result = $wpdb->get_results($query);
	if (count($result)>0)
	{
		return $result[0];
	}
	else
	{
		return false;
	}
	
	
}

function _wpr_whether_subscriber_exists($nid,$email)
{
	global $wpdb;
	$checkExistenceQuery = "SELECT * FROM ".$wpdb->prefix."wpr_subscribers WHERE email='$email' and nid='$nid'";
	$subscriberList = $wpdb->get_results($checkExistenceQuery);
	if (count($subscriberList) == 0)
		return false;
	else
		return $subscriberList[0]->id;
}


/*
	nid,
	name,
	email,
	fid,
	date,
	hash,
     
*/

function _wpr_subsciber_add_confirmed($params)
{
	global $wpdb;
	
	$nid = $params['nid'];
	$name = $params['name'];
	$email = $params['email'];
	$fid = ($params['fid'])?$params['fid']:0;
	$date = ($params['date'])?$params['date']:time();
	$hash = ($params['hash'])?$params['hash']:generateSubscriberHash();

	$checkIfExistsQuery = "SELECT count(*) number FROM ".$wpdb->prefix."wpr_subscribers WHERE email='$email' AND nid='$nid';";
	$existenceCheckResults = $wpdb->get_results($checkIfExistsQuery);
	if ($existenceCheckResults[0]->number > 0)
	{
		$updateSubscribersQuery = sprintf("UPDATE %swpr_subscribers SET nid='%s', name='%s', fid=%d, date='%s', hash='%s', active=1, confirmed=1 WHERE email='%s';",$wpdb->prefix, $name, $fid, $date, $hash, $email);
		$wpdb->query($updateSubscribersQuery);
	}
	
	$query = "INSERT INTO ".$wpdb->prefix."wpr_subscribers (nid, name, email, fid, date, hash,active, confirmed) values ('$nid','$name','$email','$fid','$date','$hash',1,1);";		
	$wpdb->query($query);
	
	
	$getSubscriberIdQuery = sprintf("SELECT id FROM %swpr_subscribers WHERE email='%s' AND nid='%s';",$wpdb->prefix,$email, $nid);
	$idResults = $wpdb->get_results($getSubscriberIdQuery);
	$id = $idResults[0]->id;
	
	return $id;
	
}

function _wpr_update_and_confirm_subscriber($nid,$name,$email)
{
    global $wpdb;
    $tableName = $wpdb->prefix."wpr_subscribers";
    $todaysDate = time();
    $updateSubscriberQuery = "UPDATE $tableName set name='$name', date='$todaysDate', confirmed=1, active=1 where email='$email' and nid=$nid";
    $wpdb->query($updateSubscriberQuery);
    $getSidQuery = "SELECT id FROM $tableName WHERE email='$email' and nid='$nid';";
    $subscriberInfo = $wpdb->get_results($getSidQuery);
    if (count($subscriberInfo) > 0 )
    {
        $id = $subscriberInfo[0]->id;
        return $id;
    }
    else
    {
        return false;
    }

}

function generateSubscriberHash()
{
	for ($i=0;$i<6;$i++)
	{
		$a[] = rand(65,90);
		$a[] = rand(97,123);
		$a[] = rand(48,57);
		
		$whichone = rand(0,2);
		$currentCharacter = chr($a[$whichone]);
		
		$hash .= $currentCharacter;
		unset($a);
		
	}
     $hash .= time();
	 return $hash;
}