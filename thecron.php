<?php
ignore_user_abort(true);
include_once "subscriber.lib.php";
include_once "blogseries.lib.php";
include_once "lib.php";
include_once "newsletter.lib.php";
function wpr_get_mailouts()
{
 	global $wpdb;
	$timeStamp = time();
	$query = "SELECT * FROM ".$wpdb->prefix."wpr_newsletter_mailouts where status = 0 and time <= $timeStamp;";
	$mailouts = $wpdb->get_results($query);
	return $mailouts;
}


/*

 *

 * $sid   - Id of the subscriber who should get the email

 * $params - The parameter array for the email that is to be sent

 *

 * $params  = array(  "subject" => String - The subject of the email.

 *                    "htmlbody"=> String - The HTML body of the email.

 *                    "textbody"=> String - The text body of the email.

 *                    "attachimages" => Boolean - Whether the images in the HTML body should be attached with the email.

 *                    "fromname"  => String - The name of the email sender
 
 		      "htmlenabled" => boolean ( 1 or 0 ) - Whether the html body of this message is enabled.

 *                    "fromemail" => String - The email address of the sender

 *

 *

 * $footerMessage - The optional footer message that is to be appended

 *                  at the bottom of the email after the email body and

 *                  before the Sender's address.

 *

 */

function make_hash($params)
{
	extract($params);
	return md5($sid.$htmlbody.$textbody.$subject);
}
 
 
 $queryList = array();
 
 
function sendmail($sid,$params,$footerMessage="")
{
	global $wpdb;
	global $queryList;
	
	$siteurl = get_bloginfo("home");
	
	$home = get_bloginfo("url");
	

	$parameters = _wpr_process_sendmail_parameters($sid,$params,$footerMessage);
	

	
	if ($parameters === false)
		return false;
		
	extract($parameters);
	
	$hash = make_hash(array_merge(array('sid'=>$sid),$parameters));
	
	$tableName = $wpdb->prefix."wpr_queue";
	
	$query = $wpdb->prepare("INSERT INTO $tableName (`from`,`fromname`, `to`, `subject`, `htmlbody`, `textbody`, `headers`,`attachimages`,`htmlenabled`,`delivery_type`,`meta_key`,`hash`,`sid`) values ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s');",
																																																 								
								$from,
								$fromname,
								$to,
								$subject,
								$htmlbody,
								$textbody,
								$headers,
								$attachImages,
								$htmlenabled,
								$delivery_type,
								$meta_key,
								$hash,
								$sid);
		$wpdb->query($query);

}

function _wpr_process_sendmail_parameters($sid, $params,$footerMessage="")
{
	$siteurl = get_bloginfo("home");
	
	$subscriber = _wpr_subscriber_get($sid);
	
	if (empty($subscriber))
		return false;
		

	$email = $subscriber->email;
	
	if (!validateEmail($email))
		return false;
		
		
	$newsletter = _wpr_newsletter_get($subscriber->nid);
    //if the fromname field is set in the newsletter, then use that value otherwise use the blog name.
	$fromname = (!empty($params['fromname']))?$params['fromname']:(!empty($newsletter->fromname))?$newsletter->fromname:get_bloginfo("name");
	
	if ($newsletter->reply_to)
		$replyto = $newsletter->reply_to;
	$unsuburl = wpr_get_unsubscription_url($sid);
	$subject = $params['subject'];
	
	$to = $email;
	$address = get_option("wpr_address");
	$textUnSubMessage = "\n\n$address\n\n".__("To unsubscribe or change subscription options visit").":\r\n\r\n$unsuburl";	
	$reply_to = $newsletter->reply_to;
	$htmlbody = trim($params['htmlbody']);
	//append the address and the unsub link to the email
	$address = "<br>
<br>
".get_option("wpr_address")."<br>
<br>
";
	$htmlUnSubscribeMessage = "<br><br>".$address."<br><br>".__("To unsubscribe or change subscriber options visit:")."<br />
\r\n <a href=\"$unsuburl\">$unsuburl</a>";
	$htmlenabled = ($params['htmlenabled'])?1:0;
	if (!empty($htmlbody))
	{
		if ($footerMessage && (!empty($htmlbody)) )
		{
			$htmlbody .= nl2br($footerMessage)."<br>
<br>
";
		}
	   $htmlbody .= $htmlUnSubscribeMessage;
	}		
	
	if ($footerMessage)
		$params['textbody'] .= $footerMessage."\n\n";
	$textbody .= $params['textbody'].$textUnSubMessage;	
	$textbody = addslashes($textbody);
	$htmlbody = addslashes($htmlbody);
	$subject = addslashes($subject);

	$delivery_type = (!empty($params['delivery_type']))?$params['delivery_type']:0;
	$email_type = (!empty($params['email_type']))?$params['email_type']:'misc';
    $attachImages = ($params['attachimages'])?1:0;
	
	$from = (!empty($params['fromemail']))?$params['fromemail']:(!empty($newsletter->fromemail))?$newsletter->fromemail:get_bloginfo('admin_email');

	$data_to_process = array($fromname, $from,  $reply_to, $textbody, $htmlbody, $subject);	
	$text_to_filter_by_custom_profile_field_plugin = implode("%$@----#---PROCESSING---#----@$%",$data_to_process);
	$text_to_filter_by_custom_profile_field_plugin = apply_filters("_wpr_generic",$text_to_filter_by_custom_profile_field_plugin,false);
	$processed_data = explode("%$@----#---PROCESSING---#----@$%",$text_to_filter_by_custom_profile_field_plugin);
	
	/*
	orignially each of these pieces were filtered individually but that caused memory fatal crashes
	thus this no-so-pretty implementation.
	*/ 
	
	$fromname = $processed_data[0];
	
	if (empty($fromname)) 
	{
		$fromname = get_bloginfo("name");	
	}
	
	$from = $processed_data[1];
	
	
	
	if (!validateEmail($from))
	{
		$from = get_bloginfo("admin_email");
	}
	
	
	
	$reply_to = $processed_data[2];
	$textbody = $processed_data[3];
	$htmlbody = $processed_data[4];
	$subject  = $processed_data[5];

	
	
	$parameters = array(
					'from'=> $from,
					'fromname' => $fromname,
					'to'=>$to,
					'reply_to'=>$reply_to,
					'subject' => $subject,
					'htmlbody'=>$htmlbody,
					'textbody' => $textbody,
					'headers'=>$headers,
					'attachimages'=>$attachImages,
					'htmlenabled'=>$htmlenabled,
					'delivery_type' => $delivery_type,
					'email_type'=>$email_type,
					'meta_key'=> $params['meta_key']
					);
	
	return $parameters;
}

function _wpr_send_and_save($sid, $params, $footerMessage="")
{
	global $wpdb;
	
	$parameters = _wpr_process_sendmail_parameters($sid,$params,$footerMessge);
	
	if ($parameters === false)
		return false;
	try {

		dispatchEmail($parameters);	
	}
	catch (Exception $excp)
	{
			
	}
	
	$queue_table_name = $wpdb->prefix."wpr_queue";
	
	$hash = make_hash(array_merge(array('sid'=>$sid),$parameters));
	
	$emailQuery = $wpdb->prepare("INSERT INTO $queue_table_name (`from`, `fromname`, `to`, `subject`, `htmlbody`, `textbody`, `headers`, `sent`, `delivery_type`, `htmlenabled`, `attachimages`, `meta_key`,`hash`,`sid`)

																VALUES

																('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s');",
									$parameters['from'],
									$parameters['fromname'],
									$parameters['to'],
									$parameters['subject'],
									$parameters['htmlbody'],
									$parameters['textbody'],
									$parameters['headers'],
									'1',
									'1',
									$parameters['htmlenabled'],
									$parameters['attachimages'],
									$parameters['meta_key'],
									$hash,
									$sid
								);
	
	$wpdb->query($emailQuery);
}
 
/*

function sendmail($sid,$params,$footerMessage="")

{

	global $hash;

	global $wpdb;

	$subscriber = _wpr_subscriber_get($sid);
	$email = $subscriber->email;
	$sitename = get_bloginfo('name');
	$siteurl = get_bloginfo('home');
	$url = get_bloginfo('siteurl');
	$newsletter = _wpr_newsletter_get($subscriber->nid);


        //if the fromname field is set in the newsletter, then use that value otherwise use the blog name.

	$fromname = (!empty($params['fromname']))?$params['fromname']:(!empty($newsletter->fromname))?$newsletter->fromname:get_bloginfo("name");
	$fromemail = get_bloginfo("admin_email");
	
	$fromname = apply_filters("_wpr_sendmail_fromname",$fromname);
	$fromemail = apply_filters("_wpr_sendmail_fromemail",$fromemail);
	
	if ($newsletter->reply_to)
	{
		$reply_to = $newsletter->reply_to;
		$reply_to = apply_filters("_wpr_sendmail_reply_to",$reply_to);
		$replytoHeader = "Reply-To: $reply_to \r\n";
	}
		
	$header = "$fromHeader $replytoHeader";	
	$unsuburl = wpr_get_unsubscription_url($sid);
	$subject = $params['subject'];

	$to = $email;
	
	$address = get_option("wpr_address");

	$textUnSubMessage = "\n\n$address\n\nTo unsubscribe or change subscriber options visit:\r\n\r\n$unsuburl";	

	$reply_to = $newsletter->reply_to;



	$htmlbody = trim($params['htmlbody']);
	//append the address and the unsub link to the email
	$address = "<br>
<br>
".$address."<br>
<br>
";
	$htmlUnSubscribeMessage = "<br><br>".$address."<br><br>To unsubscribe or change subscriber options visit:<br />
\r\n <a href=\"$unsuburl\">$unsuburl</a>";
	$htmlenabled = ($params['htmlenabled'])?1:0;
	if (!empty($htmlbody))
	{
		if ($footerMessage && (!empty($htmlbody)) )
		{
			$htmlbody .= nl2br($footerMessage)."<br>
<br>
";
		}
	   $htmlbody .= $htmlUnSubscribeMessage;
	}	

	if ($footerMessage)
		$params['textbody'] .= $footerMessage."\n\n";
		
		
	$textbody .= $params['textbody'].$textUnSubMessage;	
	$textbody = addslashes($textbody);
	$htmlbody = addslashes($htmlbody);
	$subject = addslashes($subject);

	$textbody = apply_filters("_wpr_sendmail_textbody",$textbody);
	$htmlbody = apply_filters("_wpr_sendmail_htmlbody",$htmlbody);
	$subject = apply_filters("_wpr_sendmail_subject",$subject);
	
    $attachImages = ($params['attachimages'])?1:0;



        //if the from email is set in the params, use that, if not take the newsletter's value, if that too isnt set

        //use the administrator's email address

	$from = (!empty($params['fromemail']))?$params['fromemail']:(!empty($newsletter->fromemail))?$newsletter->fromemail:get_bloginfo('admin_email');
	
	$from = apply_filters("_wpr_sendmail_from_address",$from);

	$tableName = $wpdb->prefix."wpr_queue";

	$query = "INSERT INTO $tableName (`from`,`fromname`, `to`, `subject`, `htmlbody`, `textbody`, `headers`,`attachimages`,`htmlenabled`) values ('$from','$fromname','$email','$subject','$htmlbody','$textbody','$headers','$attachImages','$htmlenabled');";

	$wpdb->query($query);

}*/



function get_postseries_posts($catid,$nid="")

{

	global $mailer;
        global $wpdb;

	$args = array(

					'post_type' => 'post',

					'numberposts' => -1,

					'category'=>$catid,

					'orderby' => 'date',

					'order' => 'ASC',

					'post_status' => 'publish'

				);
	$posts = get_posts($args);

        if (!empty($nid))
        {
                    foreach ($posts as $num=>$post)
                        {
                        $pid = $post->ID;
                        $query = "SELECT meta_value from ".$wpdb->prefix."postmeta where post_id=$pid and meta_key='wpr-options';";
                        $results = $wpdb->get_results($query);
                        $option = $results[0]->meta_value;
                        $decodedoptions = base64_decode($option);
                        $options = unserialize($decodedoptions);
                        $theRealPosts[] = $post;
                    }
        }
			
		
	return $theRealPosts;

}

function mailout_expire($id)
{
	global $wpdb;
	$query = "UPDATE ".$wpdb->prefix."wpr_newsletter_mailouts set status=1 where id=$id";
	$wpdb->query($query);
}



function get_rows($query)

{

	global $wpdb;

	return $wpdb->get_results($query);

}

function wpr_filter_query($nid, $thestring)

{	

	$sections = explode(" ",$thestring);

	$size = count($sections);

	$count=0;

	$comparisonOpers = array("equal","notequal","lessthan","greaterthan");

	$stringOperators = array("startswith","endswith","contains");

	

	$final ="";

	for ($count=0;$count<$size;)

	{

		$condition = "";

	

		if ($count != 0)

		{

			$conjunction = " ".$sections[$count]." ";

		}

		else

		{

		   $conjunction = "";

		  $count = -1; //to adjust for the indices i have used below below..

		}

		  

		$field = $sections[$count+1];

		$equality = $sections[$count+2];

		$value = $sections[$count+3];

		

	

		if (in_array($equality,$comparisonOpers))
		{			

			switch ($equality)
			{

				case 'equal':

				  $condition = "`$field` = '$value'";

				  break;

				case 'notequal':

				   $condition= "`$field` <> '$value'";

				   break;

				case 'lessthan':

				   $condition = "`$field` < '$value'";

				   break;

				case 'greaterthan':

				   $condition = "`$field` > '$value'";

			}

		}

		else if ($equality == "notnull")

		{

			$condition = "`$field` IS NOT NULL";

		}

		else if (in_array($equality,$stringOperators))

		{

			switch ($equality)

			{

				case 'startswith':

					$condition = "`$field` like '$value%'";

					break;

				case 'contains':

					$condition = "`$field` like '%$value%'";

					break;

				case 'endswith':

					$condition = "`$field` like '%$value'";

					break;

			}

		}

		else if (in_array($equality,array("before","after")) && $field == "dateofsubscription")

		{

				$thetime = strtotime($value);

				

				switch ($equality)

				{

					case 'before':

						$condition = "date < $thetime";

						break;

					case 'after':

						$condition = "date > $thetime";

						break;

				}

		}

		else if ($equality == "rlike")

		{

			$condition = "`$field` rlike '$value'"; 

		}

	

		

		$final .= $conjunction." ".$condition;

						 

		if ($count == 0) //the first element is not a conjunction

		{

			$count+=3;

		}

		else

		{

			$count +=4;

		}

	}

	return $final;

}

function _wpr_autoresponder_process($send_immediately=false)
{
	global $wpdb;
    $prefix = $wpdb->prefix;
	$process_state = get_option("_wpr_autoresponder_process_status");
	
	if ($process_state == "running") // another instance of this cron is running
		return;
		
	
	$currentTime = time();
	//$getActiveFollowupSubscriptionsQuery = "SELECT a.*, b.id sid, FLOOR(($currentTime - a.doc)/86400) daysSinceSubscribing FROM `".$prefix."wpr_followup_subscriptions` a, `".$prefix."wpr_subscribers` b  WHERE a.type='autoresponder' AND  a.sequence < FLOOR(($currentTime - a.doc)/86400) AND a.sequence <> -2 AND a.sid=b.id AND b.active=1 AND b.confirmed=1 LIMIT 1000;";
	$getActiveFollowupSubscriptionsQuery = "SELECT COUNT(*) number FROM `".$prefix."wpr_followup_subscriptions` a, `".$prefix."wpr_subscribers` b  WHERE a.type='autoresponder' AND  a.sequence < FLOOR(($currentTime - a.doc)/86400) AND a.sequence <> -2 AND a.sid=b.id AND b.active=1 AND b.confirmed=1 LIMIT 1000;";
	//$getNumberOfSubscribersQuery = "SELECT COUNT(*) number FROM `".$prefix."wpr_followup_subscriptions` a, `".$prefix."wpr_subscribers` b  WHERE a.type='autoresponder' AND  a.sequence < FLOOR(($currentTime - a.doc)/86400) AND a.sequence <> -2 AND a.sid=b.id AND b.active=1 AND b.confirmed=1;";
	$numberOfSubsRes = $wpdb->get_results($getActiveFollowupSubscriptionsQuery);
	$number = $numberOfSubsRes[0]->number;
	$max = ceil($number/1000);
	
	$start=0;
    for ($start=0;$start<$max;$start++)
	{
	    $startVal = $start*1000;
		$getSubscribersQuery = "SELECT a.*, b.id sid, FLOOR(($currentTime - a.doc)/86400) daysSinceSubscribing FROM `".$prefix."wpr_followup_subscriptions` a, `".$prefix."wpr_subscribers` b  WHERE a.type='autoresponder' AND  a.sequence < FLOOR(($currentTime - a.doc)/86400) AND a.sequence <> -2 AND a.sid=b.id AND b.active=1 AND b.confirmed=1 LIMIT $startVal, 1000;";

		$autoresponderSubscriptions = $wpdb->get_results($getSubscribersQuery);
		
		foreach ($autoresponderSubscriptions as $asubscription)
		{
			$subscriber = _wpr_subscriber_get($asubscription->sid);
			$aid = $asubscription->eid;
			$daysSinceSubscribing = floor((time()-$asubscription->doc)/86400);
			$lastSequence = $asubscription->sequence;
			
			if ($lastSequence >= $daysSinceSubscribing)
				continue;
			$query = "SELECT * FROM ".$prefix."wpr_autoresponder_messages where aid=$aid and `sequence` > $lastSequence ORDER BY `sequence` ASC LIMIT 1;";	
				
			$listOfMessages = get_rows($query);
			$autoresponder_id = $asubscription->eid;
			$subscriber_id = $asubscription->sid;
			
			if (count($listOfMessages))
			{
				$message = $listOfMessages[0];
				
				$sequence = $message->sequence;

				if ($sequence < $daysSinceSubscribing)
				{
					$newDateOfCreation = time() - ($sequence*86400);
					
					$autoresponderSubscriptionId = $asubscription->id;				
					$updateAutoresponderSubscriptionQuery = "UPDATE ".$wpdb->prefix."wpr_followup_subscriptions SET doc = '$newDateOfCreation' WHERE `id`='$autoresponderSubscriptionId';";
					$wpdb->query($updateAutoresponderSubscriptionQuery);
					$sequence = $daysSinceSubscribing;
				}
				
				
				if ($sequence != $daysSinceSubscribing)
				{
					continue;			
				}			
				
				
				

				foreach ($listOfMessages as $number=>$message)
				{								
					$subject = $message->subject;
					$subject = apply_filters("_wpr_autoresponder_email_subject",$subject);
					$textbody = $message->textbody;
					$textbody = apply_filters("_wpr_autoresponder_email_textbody",$textbody);
					$htmlbody = $message->htmlbody;
					
					$message_id = $message->id;
					
					$meta_key = sprintf("AR-%s-%s-%s",$autoresponder_id, $subscriber_id, $message_id);
					
					$emailParameters = array("subject" => $subject, 
											 "textbody" => $textbody,
											 "htmlbody" => $htmlbody,
											 "htmlenabled"=> $message->htmlenabled,
											 "attachimages"=> $message->attachimages,
											 "meta_key"=>$meta_key);
					
					wpr_place_tags($subscriber->id,$emailParameters);
					
					if ($send_immediately == false)
					{
						sendmail($subscriber->id,$emailParameters);
					}
					else
					{
						$emailParameters['delivery_type'] = 1;	
						_wpr_send_and_save($subscriber->id,$emailParameters);
					}
					
					$updateSubscriptionStatusQuery = "UPDATE `".$prefix."wpr_followup_subscriptions` SET `last_date`='".time()."', `sequence`='$message->sequence' WHERE `id`=$asubscription->id; ";
					$wpdb->query($updateSubscriptionStatusQuery);
				}
				
			}
			else
			{
				//expire that subscription
				$expireFollowupSubscriptionQuery = "UPDATE ".$prefix."wpr_followup_subscriptions set last_date='".time()."', sequence=-2 WHERE id=$asubscription->id;";
				$wpdb->query($expireFollowupSubscriptionQuery);
			}
			
		}
	
	}
	
	
	delete_option("_wpr_autoresponder_process_status");
	add_option("_wpr_autoresponder_process_status","off");
}

function wpr_processEmails()
{
	global $wpdb;
	global $mailer;
	
	
	$prefix = $wpdb->prefix;
	
	date_default_timezone_set("UTC");
	$email_mailouts= wpr_get_mailouts();
	
	
	$home = get_bloginfo('url');
	
	
	foreach ($email_mailouts as $broadcast)
	{
		$nid = $broadcast->nid;
		$subject = $broadcast->subject;
		wpr_create_temporary_tables($nid);	  //this creates the tables based on which a bigger table will be created
		wpr_make_subscriber_temptable($nid);  //this table will be used for getting the user list.
		$customFieldsConditions = trim(wpr_filter_query($nid,$broadcast->recipients));
		$customFields = ($customFieldsConditions)?" AND ".$customFieldsConditions:"";
		$query = "SELECT * FROM ".$prefix."wpr_subscribers_".$nid." where active=1 and confirmed=1 $customFields;";
		$subscribersList = $wpdb->get_results($query);
		$subject = $broadcast->subject;

		$subject = apply_filters("_wpr_broadcast_subject",$subject);

		$text_body = $broadcast->textbody;
		
		$text_body = apply_filters("_wpr_broadcast_text_body",$text_body);

		$html_body = $broadcast->htmlbody;
		
		$html_body = apply_filters("_wpr_broadcast_html_body",$html_body);
		
		$whetherToAttachImages = $broadcast->attachimages;

		$query = "SELECT fromname, fromemail from ".$wpdb->prefix."wpr_newsletters where id=".$nid;
		$results = $wpdb->get_results($query);
		$fromname = $results[0]->fromname;
		
		$fromname = apply_filters("_wpr_newsletter_fromname",$fromname);		
		$fromemail = $results[0]->fromemail;		
		$fromemail = apply_filters("_wpr_newsletter_fromemail",$fromemail);

		
		if (count($subscribersList))
		{
			$broadcastId=$broadcast->id;			
			$newsletterId= $broadcast->nid;

			foreach ($subscribersList as $subscriber)
			{
				$sid = $subscriber->id;
				

				$meta_key = sprintf("BR-%s-%s-%s",$sid,$broadcastId,$newsletterId);
				
				
				
				$email = $subscriber->email;
				
	
				$emailParameters = array( "subject" => $subject,
							  "from"=> $fromname,
							  "fromemail"=>$fromemail,
							  "textbody" => $text_body,
							  "htmlbody" => $html_body,
							  "htmlenabled"=> (empty($html_body))?0:1,
							  "attachimages"=> $whetherToAttachImages,
							  "meta_key"=> $meta_key
							  );
							  

				wpr_place_tags($sid,$emailParameters);
				
				$emailParameters["to"] = $subscriber->email;
				sendmail($sid,$emailParameters);
			}

		}
		mailout_expire($broadcast->id);
	}
	//autoresponder
	_wpr_autoresponder_process();

	//post series

	$query ="SELECT a.* FROM ".$prefix."wpr_followup_subscriptions a, ".$prefix."wpr_subscribers b where a.type='postseries' and a.sequence !='-2' and a.sid=b.id and b.active=1 and b.confirmed=1;";
	$postseriesSubscriptionList = $wpdb->get_results($query);
	foreach ($postseriesSubscriptionList as $psubscription)
	{
		if (!isPostSeriesSubscriptionActive($psubscription))
		{
			continue;
		}
		$sid = $psubscription->sid;
		$query = "SELECT nid from ".$wpdb->prefix."wpr_subscribers where id=".$sid;
		$results = $wpdb->get_results($query);
		if (count($results) != 1) //where's the newsletter?!!
			continue;
		$nid = $results[0]->nid;

		$subscriber = _wpr_subscriber_get($psubscription->sid);

		//how many days since subscribing?
		$daysSinceSubscribing = floor((time()-$psubscription->doc)/86400);
                //get the post series as an object
		$postseries = _wpr_postseries_get($psubscription->eid);
                //get the posts in the post series
        $posts = get_postseries_posts($postseries->catid,$nid);         
		$numberOfPosts = count($posts);
		$timeBetweenPosts = $postseries->frequency;
		$last_post = $psubscription->sequence;
		$currentIndex = floor($daysSinceSubscribing/$timeBetweenPosts);
		//if the post has already been sent, leave it, go to the next.
		if ($last_post >= $currentIndex) // if the posts for today have been delivered, skip further processing. 
			continue;
		
		//all posts have been sent. expire the post series subscription
		
		if ($last_post >= count($posts)-1)
		{
			
			//TODO: MAKE THIS AND THE SECTION MARKED BELOW AS A FUNCTION 
			$query = "UPDATE ".$prefix."wpr_followup_subscriptions set sequence='-2', last_date='".time()."' where id='".$psubscription->id."';";
			$wpdb->query($query);
			continue;
		}
		
		$indexToDeliver = $last_post+1;

		$category = $psubscription->eid;
		$postToSend = $posts[$indexToDeliver];

        		
		$sitename = get_bloginfo("name");
		
		$meta_key = sprintf("PS-%s-%s-%s",$psubscription->eid,$psubscription->sid,$postToSend->ID);
		
		
		$additionalParams = array('meta_key'=>$meta_key);
        deliverBlogPost($sid,$postToSend->ID,"You are receiving this blog post as a part of a post series at $name.",true,true,$additionalParams);
		$query = "UPDATE ".$prefix."wpr_followup_subscriptions set sequence=$indexToDeliver , last_date='".time()."' where id='".$psubscription->id."';";
		$wpdb->query($query);
	}
	//now process the people who subscribe to the blog
	
	$lastPostDate = get_option("wpr_last_post_date");
    $timeNow = date("Y-m-d H:i:s",time());
	$query = "SELECT * FROM ".$prefix."posts where post_type='post' and  post_status='publish' and post_date_gmt > '$lastPostDate' and post_date_gmt < '$timeNow';";
	$posts = $wpdb->get_results($query);
	
	if (count($posts) > 0 ) // are there posts being delivered? 
	{
		foreach ($posts as $post)
		{
			$query = "SELECT a.* FROM ".$prefix."wpr_subscribers a, ".$prefix."wpr_blog_subscription b where b.type='all' and a.id=b.sid and a.active=1 and a.confirmed=1;";
			$subscribers = $wpdb->get_results($query);
			//deliver this post to all subscribers of the categories of
			// this post.
			$categories = wp_get_post_categories($post->ID);
			foreach ($categories as $category)
			{
				deliver_category_subscription($category,$post);
			}
	
			if (count($subscribers) > 0)
			{
				$blogName = get_bloginfo("name");
				$blogURL = get_bloginfo("home");
				$footerMessage = "You are receiving this email because you are subscribed to the latest articles on <a href=\"$blogURL\">$blogName</a>";
				foreach ($subscribers as $subscriber)
				{
							deliverBlogPost($subscriber->id,$post->ID,$footerMessage);
				}
			}
			
			delete_option("wpr_last_post_date");
			add_option("wpr_last_post_date",$post->post_date_gmt);
			
			$sentPosts = get_option("wpr_sent_posts");
			$sentPostsList = explode(",",$sentPosts);
			$sentPostsList[] = $post->ID;
			$sentPosts = implode(",",$sentPostsList);
	
			delete_option('wpr_sent_posts');
			add_option("wpr_sent_posts",$sentPosts);	
		}
	}

	$content = ob_get_clean();

	delete_option("wpr_next_cron");
	add_option("wpr_next_cron",time()+300);

}


function isValidOptionsArray($options)
{
    if (is_array($options))
        {
        return true;
    }
    else
         return false;
}
/*
 * This function checks if the post $pid is to be skipped from being delivered to
 * subscribers of newsletter $nid.
 */

function whetherToSkipThisPost($nid,$pid)
        {
    $theoptions = get_post_meta($pid,'wpr-options',true);
    $options = unserialize($theoptions);
    if (!isset($options))
        return 0;
    //by default, the skip is disabled.
    if ($options[$nid]['disable']==1)
        {
           return 1;
    }
    else
        return 0;
}

/*

 * This function is used to generate a body for the blog post sent via email

 * when the user doesn't customize it or chooses to use the default layout

 *

 * This function is also used when the post doesn't have any WP Responder options

 * associated with it.

 * Returns string with the HTML to be used for the email

 *

 */






function getBlogContentInDefaultLayout($post_id)

{

    $post = get_post($post_id);

    $content = '<div style="background-color:  #dfdfdf;padding: 5px;"><span style="font-size: 9px; font-family: Arial; text-align:center;\">You are receiving this email because you are subscribed to new posts at ';
    $content .= "<a href=\"".get_bloginfo("home")."\">".get_bloginfo("name")."</a></span></div>";

    $content .= "
<h1><a href=\"".get_permalink($post_id)."\" style=\"font-size:22px; font-family: Arial, Verdana; text-decoration: none; color: #333399\">";

  $content .= $post->post_title;

  $content .= "</a></h1>
";

    $content .= '<p style="font-family: Arial; font-size: 10px;">Dated: '.date("d F,Y",strtotime($post->post_date));
	
	
	apply_filters("the_content",$post->post_content);

    $content .= "
</p><p><span style=\"font-family: Arial, Verdana; font-size: 12px\">".wptexturize(wpautop(nl2br($post->post_content)))."</span>

";

    $content .= "<br><br><span style=\"font-size: 12px; font-family: Arial\"><a href=\"".get_permalink($post_id)."\">Click here</a> to read this post at <a href=\"".get_bloginfo("home")."\">".get_bloginfo("name")."</a></div>.";


    return $content;



}



/*

 * This function is used to see if the subscriber with sid $sid

 * is currently receiving any follow up emails from autoresponders

 * or post series subscriptions.

 */

/*
 * This function sends the blog post with post id $post_id via email to subscriber with subscriber id $sid
 * if the the subscriber doesnt belong to a newsletter with newsletter id
 * that is in the list of newsletters that are configured to not receive this blog post.
 *
 *
 *
 */
function deliverBlogPost($sid,$post_id,$footerMessage="",$checkCondition=false,$whetherPostSeries=false,$additionalParams=array())
{
    global $wpdb;

    //get the post meta

    $sid = (int) $sid;

    $post_id = (int) $post_id;

    if ($sid == 0 || $post_id==0) // neither of these can be zero or empty.
        return;

    

    $post = get_post($post_id);
    //if plugin was activated after some posts were created

    //the options array will not exist. in that case, we just

    //deliver the blog post

    $optionsList = get_post_meta($post_id,"wpr-options",true);    
    if (!empty($optionsList))
    {
            $decoded = base64_decode($optionsList);
            $options = unserialize($decoded);
            $checkCondition = true; //if we have a valid options array, then we should
            //check the conditions of delivery.
    }
    else
    {
            $checkCondition=false;
    }
    
    $query = "SELECT nid from ".$wpdb->prefix."wpr_subscribers where id=".$sid;
    $results = $wpdb->get_results($query);
    $nid = $results[0]->nid;
    if (count($results) == 0) //if there is no subscriber by that sid
        return;

    
    $deliverFlag = true; // this flag is used to trigger the delivery
    if ($checkCondition == true)
    {
       //get the subscriber's newsletter id
        if (isset($options[$nid]))
        {
            if ($options[$nid]['disable']==1)
                {
                    $deliverFlag = false;
                }
        }
        else
            $deliverFlag=true;
   }
   else
       {
       $deliverFlag = true;
   }
   
   if (isset($additionalParams['meta_key']))
   {
	    $meta_key =  $additionalParams['meta_key'];
   }
   else
   {
		$meta_key = sprintf("BP-%s-%s",$sid,$post_id);    
   }
   //deliver the email.
   if ($deliverFlag)
   {
        //are customizations disabled? then get the html body for the blog post
        //from the default layout format.
       //check if the subscriber is currently receiving any follow up series emails
       if (isset($options) && $options[$nid]['skipactivesubscribers']==1 && isReceivingFollowupPosts($sid))
           return;

       /*
        * The conditions where the default layout is used are:
        * the customization has been disabled,
        * the customization has been disabled for post series
        * there is no customization information - the post was created when
        * wp responder was not installed/deactivated.
        */
	 
       if ($options[$nid]['nocustomization']==1 || !isValidOptionsArray($options) || ($whetherPostSeries == true && $options[$nid]['nopostseries']==1))
           {
            $htmlbody = getBlogContentInDefaultLayout($post_id);
            $post = get_post($post_id);
            $subject = $post->post_title;
			
			
			$subject = apply_filters("_wpr_blogpost_subject",$subject);			
			$htmlbody = apply_filters("_wpr_blogpost_htmlbody",$htmlbody);			
			
            $params = array("subject"=>$subject,
                            "htmlbody"=>$htmlbody,
                            "textbody"=>"",
                            "htmlenabled"=>1,
							"meta_key"=>$meta_key,
                            "attachimages"=>true);
       }
       else
       {
		     $htmlBody = $options[$nid]['htmlbody'].nl2br($footerMessage);
			 
			 $htmlEnabled = ($options[$nid]['htmlenable']==1)?1:0;
			 if (!$htmlEnabled)
			 	$htmlBody="";
				
		     $subject = $options[$nid]['subject'];
			 
 			 $subject = apply_filters("_wpr_blogpost_subject",$subject);
			 
			 $textbody = $options[$nid]['textbody'].strip_tags("$footerMessage");
			 
			 $textbody = apply_filters("_wpr_blogpost_textbody",$textbody);
			 
			 $htmlBody = apply_filters("_wpr_blogpost_htmlbody",$htmlBody);
			 
             $params = array("subject"=>$subject,
                            "htmlbody"=>$htmlBody,
                            "textbody"=>$textbody,
							"meta_key"=>$meta_key,
                            "attachimages"=>($options[$nid]['attachimages'])?1:0,
                            "htmlenabled"=> $htmlEnabled
                 );

       }

       $params['subject'] = substitutePostRelatedShortcodes($params['subject'],$post_id);
       $params['htmlbody'] = substitutePostRelatedShortcodes($params['htmlbody'],$post_id);
       $params['textbody'] = substitutePostRelatedShortcodes($params['textbody'],$post_id);
	   
	   //substitute newsletter related parameters.
	   
	   wpr_place_tags($sid,$params);
       sendmail($sid,$params);
   }

}

function substitutePostRelatedShortcodes($text,$post_id)
        {

    //the post's url
	
	
    $postUrl = get_permalink($post_id);
    $text = str_replace("[!post_url!]",$postUrl,$text);
	
    //teh post's delivery date
    //which is time right now.
    $time = date("g:iA d F Y ",time());
    $time .= date_default_timezone_get();
    $text = str_replace("[!delivery_date!]",$time,$text);
	
    //post publishing date
    $post = get_post($post_id);
    $postDate = $post->post_date;
    $postEpoch = strtotime($postDate);
    $postDate = date("dS, F Y",$postEpoch);
    $text = str_replace("[!post_date!] ",$postDate,$text);
   
    return $text;
    
}

function deliver_category_subscription($catid,$post)
{

	global $wpdb;

	$prefix = $wpdb->prefix;
	$query = "SELECT a.* FROM  ".$prefix."wpr_subscribers a,".$prefix."wpr_blog_subscription b where b.type='cat' and b.catid='$catid' and a.id=b.sid and a.active=1 and a.confirmed=1";
	$subscribers = $wpdb->get_results($query);
	$theCategory = get_category($catid);
	$categoryName = $categoryname->name;
	$blogName = get_bloginfo("name");
	$blogURL = get_bloginfo("siteurl");
	$footerMessage = "You are receiving this e-mail because you have subscribed to the $categoryName category of $blogName

$blogUrl";
	foreach ($subscribers as $subscriber)
	{
               deliverBlogPost($subscriber->id,$post->ID);
	}

}
